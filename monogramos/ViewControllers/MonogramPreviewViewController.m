//
//  MonogramPreviewViewController.m
//  monogramos
//
//  Created by lenka on 3/30/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import "MonogramPreviewViewController.h"
#import "AppManager.h"
@interface MonogramPreviewViewController ()

@end

@implementation MonogramPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSInteger num = [AppManager sharedInstance].currentMonogramNumber;
    UIImage *img = [[AppManager sharedInstance] previewForMonogramAtPosition:num];
    if (img) {
        self.preview.image = img;
    } else {
        NSString *bgName = [AppManager sharedInstance].monogramsList[num][kSavedBackground];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:bgName]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sharePreview:(id)sender {
    [self showShareMenu:@[self.preview.image] fromButton:sender completion:nil];
}

- (IBAction)deleteMonogram:(id)sender {
    [[AppManager sharedInstance] deleteCurrentMonogram];
    [self performSegueWithIdentifier:@"BackToCollectionSegue" sender:self];
}

@end
