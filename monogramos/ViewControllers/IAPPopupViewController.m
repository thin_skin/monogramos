//
//  IAPPopupViewController.m
//  monogramos
//
//  Created by ElenaD on 4/14/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import "IAPPopupViewController.h"

#import "AppManager.h"
#import "AppDelegate.h"
#import "EditorViewController.h"

@interface IAPPopupViewController ()
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIView *fontsRow;
@property (weak, nonatomic) IBOutlet UIView *wallpapersRow;
@property (weak, nonatomic) IBOutlet UIView *badgesRow;
@property (weak, nonatomic) IBOutlet UIView *removeAdsRow;

@end

@implementation IAPPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = self.popupView.frame;
    self.popupView.frame = CGRectMake(frame.origin.x, frame.origin.y + SCREEN_HEIGHT, frame.size.width, frame.size.height);
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:1 animations:^{
        weakSelf.popupView.frame = frame;
    }];
    
    // set actual prices
    for (Product *product in [AppManager sharedInstance].products.allValues) {
        if([product.productIdentifier isEqualToString:kPurchaseID_Fonts]) {
            if (product.isAvailable) self.fontsRow.alpha = 0.4;
            self.fontsTitle.text = product.localizedTitle;
            self.fontsDescription.text = product.localizedDescription;
            self.fontsPrice.text = product.localizedPrice;
        } else if([product.productIdentifier isEqualToString:kPurchaseID_Wallpapers]) {
            if (product.isAvailable) self.wallpapersRow.alpha = 0.4;
            self.wallpapersTitle.text = product.localizedTitle;
            self.wallpapersDescription.text = product.localizedDescription;
            self.wallpapersPrice.text = product.localizedPrice;
        } else if([product.productIdentifier isEqualToString:kPurchaseID_Badges]) {
            if (product.isAvailable) self.badgesRow.alpha = 0.4;
            self.shapesTitle.text = product.localizedTitle;
            self.shapesDescription.text = product.localizedDescription;
            self.shapesPrice.text = product.localizedPrice;
        } else if([product.productIdentifier isEqualToString:kPurchaseID_AdsFree]) {
            if (product.isAvailable) self.removeAdsRow.alpha = 0.4;
            self.removeAdsTitle.text = product.localizedTitle;
            self.removeAdsDescription.text = product.localizedDescription;
            self.removeAdsPrice.text = product.localizedPrice;
        } else if([product.productIdentifier isEqualToString:kPurchaseID_PRO]) {
            self.buyAllTitle.text = [product.localizedTitle uppercaseString];
            self.buyAllPrice.text = product.localizedPrice;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closePopup:) name:kPurchaseCompleteNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPurchaseCompleteNotification object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)restorePurchases:(id)sender {
    [[AppManager sharedInstance] restorePurchases];
}

- (void)purchaseIfUnavailable:(NSString*)purchaseId {
    Product *prod = [AppManager sharedInstance].products[purchaseId];
    if (prod.isAvailable) {
        [self showMessage:[NSString stringWithFormat:@"%@ is already purchased", prod.localizedTitle] withTitle:nil withAction:nil];
        return;
    }
    [[AppManager sharedInstance] purchase:purchaseId];
}

- (IBAction)buyFonts:(id)sender {
    [self purchaseIfUnavailable:kPurchaseID_Fonts];
}
- (IBAction)buyWallpapers:(id)sender {
    [self purchaseIfUnavailable:kPurchaseID_Wallpapers];
}
- (IBAction)buyBadges:(id)sender {
    [self purchaseIfUnavailable:kPurchaseID_Badges];
}
- (IBAction)removeAds:(id)sender {
    [self purchaseIfUnavailable:kPurchaseID_AdsFree];
}
- (IBAction)buyAll:(id)sender {
    [self purchaseIfUnavailable:kPurchaseID_PRO];
}
- (IBAction)closePopup:(id)sender {
    __weak typeof(self) weakSelf = self;
    CGRect frame = self.popupView.frame;
    [UIView animateWithDuration:1 animations:^{
        weakSelf.popupView.frame = CGRectMake(frame.origin.x, frame.origin.y + SCREEN_HEIGHT, frame.size.width, frame.size.height);
    } completion:^(BOOL finished) {
        weakSelf.navigationController.navigationBarHidden = NO;
        [weakSelf.view removeFromSuperview];
        [weakSelf removeFromParentViewController];
    }];
    ((EditorViewController*)self.parentViewController).isIAPPopupShown = NO;
}

@end
