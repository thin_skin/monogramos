//
//  MonogramPreviewViewController.h
//  monogramos
//
//  Created by lenka on 3/30/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MonogramPreviewViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *preview;

@end
