//
//  EditorViewPresenter.h
//  monogramos
//
//  Created by lenka on 3/27/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EditorViewDataSource.h"

@class EditorViewController;

@interface EditorViewPresenter : NSObject <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak) EditorViewController *vc;

@property FontsCollectionDataSource *fontsDataSource;
@property ColorsCollectionDataSource *colorsDataSource;
@property BackgroundColorsCollectionDataSource *backgroundColorsDataSource;
@property BackgroundsCollectionDataSource *backgroundsDataSource;
@property BackgroundCategoriesCollectionDataSource *backgroundCategoriesDataSource;
@property ShapesCollectionDataSource *shapesDataSource;
@property ShapeColorsCollectionDataSource *shapeColorDataSource;

@property NSInteger currentBackgroundCategoryIndex;
@property NSInteger selectedBackgroundCategoryIndex;
@property NSInteger selectedBackgroundIndex;
@property NSInteger selectedShapeIndex;
@property NSInteger selectedFontIndex;

@property UIColor *selectedBackgroundColor;
@property UIColor *selectedShapeColor;

- (NSString*)currentBackgroundPatternName;
- (NSString*)currentShapeName;

@property UIImage *currentBackgroundPattern;
@property UIImage *currentShape;

+ (instancetype)presenterForVC:(EditorViewController *)vc;
- (void)setDataSources;

- (void)selectTextColorAtIndex:(NSUInteger)index;
- (void)setTextToMonogram;
- (void)correctMonogramLabelXPosition;

- (void)selectFontAtIndex:(NSUInteger)index;
- (void)selectBackgroundCategoryAtIndex:(NSUInteger)index;
- (void)selectBackgroundAtIndex:(NSUInteger)index;
- (void)selectBackgroundColorAtIndex:(NSUInteger)index;
- (void)selectShapeAtIndex:(NSUInteger)index;
- (void)selectShapeColorAtIndex:(NSUInteger)index;

- (BOOL)onTextFieldChangeToString:(NSString*)newString byTypingString:(NSString*)string;

- (void)saveCreation;
- (void)initCurrentValues;
- (void)updateFontForSize:(CGFloat)size;
@end
