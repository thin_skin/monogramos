//
//  ViewController.m
//  monogramos
//
//  Created by lenka on 2/25/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import "EditorViewController.h"
#import "EditorViewPresenter.h"
#import "AppManager.h"

#import "CircleMonogramsFont.h"
#import "NSString+checkTextWidth.h"
#import "IAPPopupViewController.h"

#pragma mark - ViewController
@interface EditorViewController ()

@end

@implementation EditorViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.presenter = [EditorViewPresenter presenterForVC:self];
    
    [self.presenter setDataSources];
    [self.presenter initCurrentValues];

   
    if (self.monogramLabel.text.length == 0) {
        self.mode = kAppModeEditText;
        [self showMenuForCurrentMode];
    }

    [[UISlider appearance] setThumbImage:[UIImage imageNamed:@"bar_btn.png"] forState:UIControlStateNormal];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.navigationController.navigationBarHidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkRecoloringAvailability) name:kNotificationMainColorsUpdate object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkRecoloringAvailability {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.mode == kAppModeSelectWallpaper) {
            id color = [[AppManager sharedInstance] mainColorOfPattern:weakSelf.presenter.currentBackgroundPatternName];
            [weakSelf switchRecoloringPanel:weakSelf.backgroundColorsCollection ifMainColor:color];
        } else if (self.mode == kAppModeSelectShape) {
            id color = [[AppManager sharedInstance] mainColorOfShape:weakSelf.presenter.currentShapeName];
            [weakSelf switchRecoloringPanel:weakSelf.shapeColorsCollection ifMainColor:color];
        }
    });
}
- (IBAction)fontScale:(UISlider*)slider {
    CGFloat newSize = kMinFontSize + slider.value * (kMaxFontSize - kMinFontSize);
    [self.presenter updateFontForSize:newSize];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return [self.presenter onTextFieldChangeToString:newString byTypingString:string];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.presenter onTextFieldChangeToString:textField.text byTypingString:nil]) {
        [self hideKeyboard];
        return NO;
    }
    return YES;
}

- (IBAction)onMonogramTextEdit:(UITextField*)sender {
    [self.presenter setTextToMonogram];
}

- (void)showMenuForCurrentMode {
    if (self.mode == kAppModeSelectMainToolbar) {
        self.mainToolbar.hidden = NO;
        
    } else if (self.mode == kAppModeSelectWallpaper) {
        self.mainToolbar.hidden = YES;
        self.backgroundsToolbar.hidden = NO;
        [self checkRecoloringAvailability];

    } else if (self.mode == kAppModeSelectShape) {
        self.shapesToolbar.hidden = NO;
        [self checkRecoloringAvailability];

    } else if (self.mode == kAppModeSelectFont) {
        self.mainToolbar.hidden = YES;
        self.fontsToolbar.hidden = NO;

    } else if (self.mode == kAppModeEditText) {
        [self openKeyboard:nil];
    }
    
    self.toolbarShapesButton.selected = (self.mode == kAppModeSelectShape);
}

- (void)hideUselessMenus {
    if (self.mode != kAppModeEditText) {
        [self hideKeyboard];
    }
    
    if (self.mode != kAppModeSelectWallpaper) {
        self.backgroundsToolbar.hidden = YES;
    }
    
    if (self.mode != kAppModeSelectShape) {
        self.shapesToolbar.hidden = YES;
    }
    
    if (self.mode != kAppModeSelectFont) {
        self.fontsToolbar.hidden = YES;
    }

    if ( self.mode == kAppModeSelectWallpaper || self.mode == kAppModeSelectFont) {
        self.mainToolbar.hidden = YES;
    }
    
    if (self.mode == kAppModeSelectWallpaper || self.mode == kAppModeSelectShape) {
        [self checkRecoloringAvailability];
    }
}

- (IBAction)backToMainToolbar:(id)sender {
    self.mode = kAppModeSelectMainToolbar;
    [self hideUselessMenus];
    [self showMenuForCurrentMode];
}

#pragma mark - main toolbar action
- (IBAction)showBackgroundsBar:(id)sender {
    self.mode = kAppModeSelectWallpaper;
    [self hideUselessMenus];
    [self showMenuForCurrentMode];
}

- (IBAction)showShapesBar:(id)sender {
    self.mode = kAppModeSelectShape;
    [self hideUselessMenus];
    [self showMenuForCurrentMode];
}

- (IBAction)showFontsBar:(id)sender {
    self.mode = kAppModeSelectFont;
    [self hideUselessMenus];
    [self showMenuForCurrentMode];
}

- (IBAction)openKeyboard:(id)sender {
    self.mode = kAppModeEditText;
    [self.monogramTextInput becomeFirstResponder];
}

- (IBAction)upgradeToPro:(id)sender {
    [self showIAPPopup];
}

- (void)hideKeyboard {
    [self.monogramTextInput resignFirstResponder];
}

- (void)switchRecoloringPanel:(UICollectionView*)panel ifMainColor:(id)mainColor {
    BOOL isShown = [mainColor isKindOfClass:[UIColor class]];
    panel.hidden = !isShown;
}

- (void)hideRecoloringPanel {
    self.backgroundColorsCollection.hidden = YES;
}

- (void)creationShouldBeSaved {
    [self.presenter saveCreation];
}

- (void)discardChanges {
    __weak typeof(self) weakSelf = self;
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf performSegueWithIdentifier:@"DiscardChangesUnwindSegue" sender:nil];
    }];
    [self showMessage:@"" withTitle:@"Are you sure you want to exit without saving your work?" withAction:okAction];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"DiscardChangesUnwindSegue"]) {
        [self discardChanges];
        return NO;
    }
    return YES;
}

#pragma mark - UICollectionViews delegate method
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.backgroundCategoriesCollection) {
        [self.presenter selectBackgroundCategoryAtIndex:indexPath.item];
        
    } else if (collectionView == self.fontColorsCollection) {
        [self.presenter selectTextColorAtIndex:indexPath.item];
        
    } else if (collectionView == self.fontsCollection) {
        [self.presenter selectFontAtIndex:indexPath.item];
        [self.presenter correctMonogramLabelXPosition];
        
    } else if (collectionView == self.backgroundsCollection) {

        [self.presenter selectBackgroundAtIndex:indexPath.item];
        
    } else if (collectionView == self.backgroundColorsCollection) {
        
        [self.presenter selectBackgroundColorAtIndex:indexPath.item];
        
    } else if (collectionView == self.shapesCollection) {

        [self.presenter selectShapeAtIndex:indexPath.item];
    
    } else if (collectionView == self.shapeColorsCollection) {
        
        [self.presenter selectShapeColorAtIndex:indexPath.item];
    }
    
    // update borders for selected items
    [collectionView reloadData];
}


#pragma mark - full screen on tap
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (self.isIAPPopupShown) return;
    if (self.navigationController.isNavigationBarHidden) {
        [self hideMainToolbars:NO];
        [self showMenuForCurrentMode];
    } else {
        [self hideAllBars];
    }
}

- (void)hideMainToolbars:(BOOL)isHidden {
    self.mainToolbar.hidden = isHidden;
    self.navigationController.navigationBarHidden = isHidden;
    self.bottomAd.hidden = isHidden;
}

- (void)hideAllBars {
    [self hideMainToolbars:YES];
    
    self.backgroundsToolbar.hidden = YES;
    self.shapesToolbar.hidden = YES;
    self.fontsToolbar.hidden = YES;
    
    [self hideKeyboard];
}

- (void) showIAPPopup {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    IAPPopupViewController *adsVC = (IAPPopupViewController*)[storyboard instantiateViewControllerWithIdentifier:@"IAPPopup"];
    
    [self addChildViewController:adsVC];
    [self.view addSubview:adsVC.view];
    [adsVC didMoveToParentViewController:self];

    UINavigationController *navVC = self.navigationController;
    navVC.navigationBarHidden = YES;
    self.isIAPPopupShown = YES;
}

@end
