//
//  BaseViewController.h
//  monogramos
//
//  Created by lenka on 4/8/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (void)showMessage:(NSString*_Nonnull)str withTitle:(NSString*__nullable)title
         withAction:(UIAlertAction*_Nullable)action;
- (void)showShareMenu:(NSArray*_Nonnull)shareItems
           fromButton:(UIBarButtonItem*_Nonnull)barButton
           completion:(void (^ __nullable)(void))completion;

@end
