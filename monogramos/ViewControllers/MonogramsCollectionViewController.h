//
//  MonogramsCollectionViewController.h
//  monogramos
//
//  Created by lenka on 3/30/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MonogramsCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *preview;
@property (weak, nonatomic) IBOutlet UIImageView *selectionSign;
@end

@interface MonogramsCollectionViewController : BaseViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) IBOutlet UIView *noDesignsView;
@property (weak, nonatomic) IBOutlet UICollectionView *previewsCollectionView;

// controls for selection mode only
@property (weak, nonatomic) IBOutlet UIToolbar *toolbarForSelected;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editBarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelBarButton;
// plus sign - for empty list only
@property (strong, nonatomic) IBOutlet UIBarButtonItem *plusBarButton;

@property BOOL isSelectionMode;
@property NSMutableArray *selectedIndexes;
@end
