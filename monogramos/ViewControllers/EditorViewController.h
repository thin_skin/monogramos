//
//  EditorViewController.h
//  monogramos
//
//  Created by lenka on 2/25/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class EditorViewPresenter;

typedef enum {
    kAppModeSelectMainToolbar = 0,
    kAppModeSelectWallpaper,
    kAppModeSelectShape,
    kAppModeSelectFont,
    kAppModeEditText,
    kAppModeCount
} kAppMode;



@interface EditorViewController : BaseViewController <UITextFieldDelegate, UICollectionViewDelegate>

@property int mode;
@property BOOL isIAPPopupShown;
@property EditorViewPresenter *presenter;

@property (weak, nonatomic) IBOutlet UILabel *monogramLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *xCenterConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint;

@property (weak, nonatomic) IBOutlet UITextField *monogramTextInput;
@property (weak, nonatomic) IBOutlet UIView *mainToolbar;
@property (weak, nonatomic) IBOutlet UIButton *toolbarBgButton;
@property (weak, nonatomic) IBOutlet UIButton *toolbarShapesButton;
@property (weak, nonatomic) IBOutlet UIButton *toolbarFontsButton;
@property (weak, nonatomic) IBOutlet UIButton *toolbarKeyboardButton;
@property (weak, nonatomic) IBOutlet UIButton *toolbarUpgradeButton;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *shapeImageView;

@property (weak, nonatomic) IBOutlet UICollectionView *backgroundCategoriesCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *backgroundsCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *backgroundColorsCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *shapesCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *shapeColorsCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *fontsCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *fontColorsCollection;

@property (weak, nonatomic) IBOutlet UIView *backgroundsToolbar;
@property (weak, nonatomic) IBOutlet UIView *shapesToolbar;
@property (weak, nonatomic) IBOutlet UIView *fontsToolbar;
@property (weak, nonatomic) IBOutlet UISlider *fontsizeSlider;

@property (weak, nonatomic) IBOutlet UIView *bottomAd;

- (void)checkRecoloringAvailability;
- (void)hideRecoloringPanel;
- (void)hideMainToolbars:(BOOL)isHidden;
- (void)hideAllBars;
- (void)creationShouldBeSaved;

- (void) showIAPPopup;

@end

