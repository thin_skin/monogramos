//
//  MonogramsCollectionViewController.m
//  monogramos
//
//  Created by lenka on 3/30/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import "MonogramsCollectionViewController.h"
#import "AppManager.h"

#import "EditorViewController.h"

#define kCollectionCellSpacing (IS_IPAD ? 40 : 20);

@implementation MonogramsCollectionCell
- (void)prepareForReuse {
    self.selectionSign.hidden = YES;
}
@end

@implementation MonogramsCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.previewsCollectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"layout_bg.pn6g"]];

    [self updateNavigationItem];
}


- (void)reloadCollectionData {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.previewsCollectionView reloadData];
    });
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadCollectionData) name:kNotificationMonogramPreviewUpdate object:nil];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)updateNavigationItem {
    if ([AppManager sharedInstance].monogramsList.count == 0) {
        self.noDesignsView.hidden = NO;
        self.navigationItem.leftBarButtonItems = nil;
        self.navigationItem.rightBarButtonItems = nil;
    } else {
        if (self.isSelectionMode) {
            self.selectedIndexes = [@[] mutableCopy];
            self.navigationItem.leftBarButtonItems = @[self.cancelBarButton];
            self.navigationItem.rightBarButtonItem = nil;
        } else {
            self.noDesignsView.hidden = YES;
            self.navigationItem.leftBarButtonItems = @[self.editBarButton];
            self.navigationItem.rightBarButtonItems = @[self.plusBarButton];
        }
    }
    [self updateNavigationTitle];
}

- (void)updateNavigationTitle {
    NSString *title;
    if (self.isSelectionMode) {
        if (self.selectedIndexes.count > 0) {
            title = [NSString stringWithFormat:@"%ld items selected", (long)self.selectedIndexes.count];
        } else {
            title = @"Select items";
        }
    } else {
        title = @"My Monogram";
    }
    self.navigationItem.title = title;
}

- (IBAction)switchMode:(UIBarButtonItem*)sender {
    self.isSelectionMode = (sender == self.editBarButton);
    self.toolbarForSelected.hidden = !self.isSelectionMode;
    [self updateNavigationItem];
    [self reloadCollectionData];
}

#pragma mark - bunch operations for selected items
- (IBAction)shareSelectedItems:(id)sender {
    if (self.selectedIndexes.count == 0) return;
    
    NSMutableArray *selectedPreviews = [@[] mutableCopy];
    for (NSNumber *num in _selectedIndexes) {
        NSUInteger indexArr[] = {0, num.intValue};
        NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexArr length:2];
        MonogramsCollectionCell *cell = (MonogramsCollectionCell*)[self.previewsCollectionView cellForItemAtIndexPath:indexPath];
        [selectedPreviews addObject:cell.preview.image];
    }
    __weak typeof(self) weakSelf = self;
    [self showShareMenu:selectedPreviews fromButton:sender completion:^{
        // exit selection mode
        [weakSelf switchMode:nil];
    }];
}

- (IBAction)deleteSelectedItems:(id)sender {
    if (self.selectedIndexes.count == 0) return;

    [[AppManager sharedInstance] deleteMonogramsAtPositions:self.selectedIndexes];
    // exit selection mode
    [self switchMode:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [AppManager sharedInstance].monogramsList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MonogramsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MonogramsCollectionCell" forIndexPath:indexPath];

    UIImage *img = [[AppManager sharedInstance] previewForMonogramAtPosition:indexPath.item];
    NSString *shapeName = [AppManager sharedInstance].monogramsList[indexPath.item][kSavedShape];
    cell.preview.image = img ? img : [UIImage imageNamed:shapeName];

    if (self.isSelectionMode &&
        [self.selectedIndexes containsObject:@(indexPath.item)]) {
        cell.selectionSign.hidden = NO;
    }
    return cell;
}

#pragma mark - <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isSelectionMode) {
        MonogramsCollectionCell *cell = (MonogramsCollectionCell*)[self.previewsCollectionView cellForItemAtIndexPath:indexPath];
        NSNumber *ind = @(indexPath.item);
        if ([self.selectedIndexes containsObject:ind]) {
            [self.selectedIndexes removeObject:ind];
            cell.selectionSign.hidden = YES;
            
        } else {
            [self.selectedIndexes addObject:ind];
            cell.selectionSign.hidden = NO;
        }
        [self updateNavigationTitle];
        
    } else {
        // before segue (open preview) triggering
        [AppManager sharedInstance].currentMonogramNumber = indexPath.item;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize itemSize = IS_IPAD ? CGSizeMake(324, 432) : CGSizeMake(130, 230);
    if (IS_IPAD_PRO) itemSize = CGSizeMake(288, 384);
    return itemSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    float inset = kCollectionCellSpacing;
    return UIEdgeInsetsMake(inset, inset, inset, inset);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return kCollectionCellSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return kCollectionCellSpacing;
}

#pragma mark - prepare for segue
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return !self.isSelectionMode;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AddNewMonogramSegue"]) {
        [AppManager sharedInstance].currentMonogramNumber = -1;
    }
}

- (IBAction)prepareForUnwind:(UIStoryboardSegue*)segue {
    if ([segue.identifier isEqualToString:@"SaveChangesUnwindSegue"]) {
        [(EditorViewController*)segue.sourceViewController creationShouldBeSaved];
    }
    [self updateNavigationItem];
    [self reloadCollectionData];
}

@end
