//
//  EditorViewPresenter.m
//  monogramos
//
//  Created by lenka on 3/27/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import "EditorViewPresenter.h"

#import "EditorViewController.h"

#import "AppManager.h"

#import "CircleMonogramsFont.h"
#import "CALayer+GetUIImage.h"
#import "UIImage+ReplaceColor.h"
#import "TextChecker.h"
#import "NSString+checkTextWidth.h"
#import "IAPPopupViewController.h"

@implementation EditorViewPresenter

+ (instancetype)presenterForVC:(EditorViewController *)vc {
    EditorViewPresenter *instance = [[self class] new];
    instance.vc = vc;
    return instance;
}


- (instancetype)init {
    if (self = [super init]) {
        self.backgroundCategoriesDataSource = [[BackgroundCategoriesCollectionDataSource alloc] initWithCategories:[AppManager sharedInstance].categories];
        self.backgroundsDataSource = [BackgroundsCollectionDataSource new];
        
        self.backgroundColorsDataSource = [BackgroundColorsCollectionDataSource new];

        self.shapesDataSource = [ShapesCollectionDataSource new];
        self.shapesDataSource.freeCount = [[AppManager sharedInstance].paidContentData[kContentPlistPaidDataBadgesKey][kContentPlistPaidDataFreeCount] integerValue];
        self.shapeColorDataSource = [ShapeColorsCollectionDataSource new];

        self.fontsDataSource = [FontsCollectionDataSource new];
        self.fontsDataSource.freeCount = [[AppManager sharedInstance].paidContentData[kContentPlistPaidDataFontsKey][kContentPlistPaidDataFreeCount] integerValue];
        
        self.colorsDataSource = [ColorsCollectionDataSource new];
        self.selectedBackgroundIndex = -1;
        self.selectedShapeIndex = -1;
        
        // IAPurchases
        [self unlockPurchasedContent];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPurchaseComplete:) name:kPurchaseCompleteNotification object:nil];
        
    }
    return self;
}

#pragma mark - initialize the data sources
- (void)setDataSources {
    self.vc.backgroundsCollection.dataSource = self.backgroundsDataSource;
    self.vc.backgroundColorsCollection.dataSource = self.backgroundColorsDataSource;
    self.vc.backgroundCategoriesCollection.dataSource = self.backgroundCategoriesDataSource;
    self.vc.fontsCollection.dataSource = self.fontsDataSource;
    self.vc.fontColorsCollection.dataSource = self.colorsDataSource;
    self.vc.shapesCollection.dataSource = self.shapesDataSource;
    self.vc.shapeColorsCollection.dataSource = self.shapeColorDataSource;
}

#pragma mark - update text (preprocessing)
- (void)setTextToMonogram {
    NSString *monogramOriginalText = self.vc.monogramTextInput.text;
    NSString *monogramFontname = self.currentFontName;
    
    if ([CircleMonogramsFont isOwnFontWithName:monogramFontname]) {
        [self setCircleMonogramText:monogramOriginalText withFont:monogramFontname];
    } else {
        self.vc.monogramLabel.text = monogramOriginalText;
    }
    [self correctMonogramLabelWidth];
    [self correctMonogramLabelXPosition];
}

- (void)correctMonogramLabelWidth {
    if ([CircleMonogramsFont isOwnFontWithName:self.currentFontName]) {
        if ([self updateConstraintForWidth]) [self.vc.view setNeedsLayout];
    } else {
        if ([self resetConstraints]) [self.vc.view setNeedsLayout];
    }
}

- (void)correctMonogramLabelXPosition {
    if ([CircleMonogramsFont isOwnFontWithName:self.currentFontName]) {
        if ([self updateConstraintForXPosition]) [self.vc.view setNeedsLayout];
    } else {
        if ([self resetConstraints]) [self.vc.view setNeedsLayout];
    }
}


- (void)setCircleMonogramText:(NSString*)text withFont:(NSString*)CMFontname {
    NSString *currentLabelFontName = self.vc.monogramLabel.font.fontName;
    if (text.length > [CircleMonogramsFont maxLength] &&
        ![CircleMonogramsFont isOwnFontWithName:currentLabelFontName]) {
        [self.vc showMessage:@"Some text was hidden to match the style of this font." withTitle:nil withAction:nil];
    }
        
    kFrameType frameType = [CircleMonogramsFont frameTypeFromEncodedFontname:CMFontname];
    self.vc.monogramLabel.text = [CircleMonogramsFont prepareString:text withFrame:frameType];
    
    kBackgroundType type = [CircleMonogramsFont typeFromEncodedFontname:CMFontname];
    NSString *actualFontname = [CircleMonogramsFont fontnameForString:text withBackgroundType:type];
    self.vc.monogramLabel.font = [UIFont fontWithName:actualFontname size:self.vc.monogramLabel.font.pointSize];
}

- (BOOL)updateConstraintForWidth {
    CGFloat size = [NSString getActualFontSizeForLabel:self.vc.monogramLabel];
    CGFloat widthDelta = size * 0.3;
    if (self.vc.monogramTextInput.text.length == 0) { widthDelta *= 0; }
    else if (self.vc.monogramTextInput.text.length == 1) { widthDelta *= -4; }
    else if (self.vc.monogramTextInput.text.length == 2) { widthDelta *= 0; }
    else if (self.vc.monogramTextInput.text.length > 2) { widthDelta *= 2.2; }
    if (self.vc.widthConstraint.constant != widthDelta) {
        self.vc.widthConstraint.constant = widthDelta;
        return YES;
    }
    return NO;
}

- (BOOL)updateConstraintForXPosition {
    CGFloat size = [NSString getActualFontSizeForLabel:self.vc.monogramLabel];
    CGFloat offset = size * 0.3;
    if (self.vc.monogramTextInput.text.length == 0) { offset *= -2; }
    else if (self.vc.monogramTextInput.text.length == 1) { offset *= -1; }
    else if (self.vc.monogramTextInput.text.length == 2) { offset = 0; }
    else if (self.vc.monogramTextInput.text.length > 2) { }
    if (self.vc.xCenterConstraint.constant != offset) {
        self.vc.xCenterConstraint.constant = offset;
        return YES;
    }
    return NO;
}

- (BOOL)resetConstraints {
    BOOL res = NO;
    if (self.vc.xCenterConstraint.constant != 0) {
        self.vc.xCenterConstraint.constant = 0;
        res = YES;
    }
    if (self.vc.widthConstraint.constant != 0) {
        self.vc.widthConstraint.constant = 0;
        res = YES;
    }
    return res;
}

#pragma mark - select item from collections
// color (for font)
- (void)selectTextColorAtIndex:(NSUInteger)index {
    self.vc.monogramLabel.textColor = self.colorsDataSource.colors[index];
}

// font
- (NSString*)currentFontName {
    return _fontsDataSource.fonts[_selectedFontIndex];
}

- (void)selectFontAtIndex:(NSUInteger)index  {
    if (index == 0) {
        // remove text
        self.vc.monogramTextInput.text = @"";
        [self setTextToMonogram];
    } else {
        if ([self.fontsDataSource isFreeFontAtIndex:index - 1]) {
            [self setFontIndex:index - 1];
        } else {
            [self.vc showIAPPopup];
        }
    }
}

- (void)setFontIndex:(NSUInteger)index {
    self.selectedFontIndex = index;
    self.fontsDataSource.currentIndex = index;
    
    NSString *monogramFontname = self.currentFontName;
    NSString *monogramOriginalText = self.vc.monogramTextInput.text;
    
    if ([CircleMonogramsFont isOwnFontWithName:monogramFontname]) {
        [self setCircleMonogramText:monogramOriginalText withFont:monogramFontname];
    } else {
        self.vc.monogramLabel.text = monogramOriginalText;
        self.vc.monogramLabel.font = [UIFont fontWithName:monogramFontname size:self.vc.monogramLabel.font.pointSize];
    }
}

// wallpaper categories
- (void)setBackgroundCategoryWithIndex:(NSUInteger)index {
    if (![self.backgroundCategoriesDataSource isFreeCategoryAtNumber:index]) {
        index = self.currentBackgroundCategoryIndex;
    }

    NSString *patternName = self.backgroundCategoriesDataSource.categories[index][kContentPlistCategoryPatternKey];
    NSNumber *maxIndex = self.backgroundCategoriesDataSource.categories[index][kContentPlistCategoryMaxIndexKey];
    NSNumber *freeCount = self.backgroundCategoriesDataSource.categories[index][kContentPlistCategoryFreeCountKey];

    if ([self.backgroundCategoriesDataSource isFreeCategoryAtNumber:index]) {
        self.currentBackgroundCategoryIndex = index;
        self.backgroundCategoriesDataSource.currentIndex = index;
        
        [self.backgroundsDataSource setCategoryName:patternName withMaxIndex:maxIndex withFreeCount:freeCount];
        [self.vc.backgroundsCollection reloadData];
        [self.vc.backgroundsCollection setContentOffset:CGPointMake(0, 0)];
    }
}

- (void)selectBackgroundCategoryAtIndex:(NSUInteger)index {
    if ([self.backgroundCategoriesDataSource isFreeCategoryAtNumber:index]) {
        [self setBackgroundCategoryWithIndex:index];
    } else {
        [self.vc showIAPPopup];
    }
}

// wallpaper
- (void)pickPhotoFromPhotoLibrary {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        [self.vc presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    self.vc.backgroundImageView.backgroundColor = [UIColor clearColor];
    [self setBackgroundPhoto:image];
    
    [self.vc dismissViewControllerAnimated:YES completion:nil];
}

- (void)setBackgroundPhoto:(UIImage*)photoFromLibrary {
    self.vc.backgroundImageView.image = photoFromLibrary;
}

- (NSString*)currentBackgroundPatternName {
    return (_selectedBackgroundIndex >= 0) ? _backgroundsDataSource.patterns[_selectedBackgroundIndex] : nil;
}

- (void)selectBackgroundAtIndex:(NSUInteger)index {
    if (index == kWallpapersCollectionAddPhotoIndex) {
        // select from PhotoLibrary
        [self setBackgroundPatternWithIndex:-1];
        [self pickPhotoFromPhotoLibrary];
        [self.vc hideRecoloringPanel];
    } else if (index ==  kWallpapersCollectionRemoveIndex) {
        // erase background
        [self setBackgroundPatternWithIndex:-1];
        [self.vc hideRecoloringPanel];
    } else {
        // set background
        NSUInteger realIndex = [self.backgroundsDataSource realPatternIndexForIndex:index];
        if ([self.backgroundsDataSource isFreePatternAtIndex:realIndex]) {
            UIImage *backgroundImage = [self setBackgroundPatternWithIndex:realIndex];
            [self.vc checkRecoloringAvailability];
            [[AppManager sharedInstance] checkMainColorOfPattern:backgroundImage withName:self.currentBackgroundPatternName];
            
        } else {
            [self.vc showIAPPopup];
        }
    }
}

// wallpapers
- (UIImage*)setBackgroundPatternWithIndex:(NSUInteger)index {
    self.selectedBackgroundIndex = index;
    self.backgroundsDataSource.currentIndex = index;
    
    self.selectedBackgroundCategoryIndex = self.currentBackgroundCategoryIndex;
    self.backgroundsDataSource.isForSelectedCategory = YES;
    
    self.selectedBackgroundColor = nil;
    
    // erase if it was photo from library
    [self setBackgroundPhoto:nil];
    
    if (index == -1) {
        // remove any background
        self.selectedBackgroundIndex = -1;
        self.vc.backgroundImageView.backgroundColor = [UIColor clearColor];
        return nil;
        
    } else {
        
        NSString *backgroundName = self.currentBackgroundPatternName;
        if (!backgroundName) return nil;
        UIImage *image = [UIImage imageNamed:backgroundName];
        // set pattern
        self.vc.backgroundImageView.backgroundColor = [UIColor colorWithPatternImage:image];
        self.currentBackgroundPattern = image;
        return image;
    }
}

// color (for wallpaper recoloring)
- (void)selectBackgroundColorAtIndex:(NSUInteger)index {
    if (index == 0) {
        // revoke all recoloring
        self.selectedBackgroundColor = nil;
        self.vc.backgroundImageView.backgroundColor = [UIColor colorWithPatternImage:self.currentBackgroundPattern];
    } else {
        // recolor
        UIColor *color = self.backgroundColorsDataSource.colors[index - 1];
        self.selectedBackgroundColor = color;
        [self backgroundColorsReplace];
    }
}

- (NSString*)currentShapeName {
    return (_selectedShapeIndex >= 0) ? _shapesDataSource.shapes[_selectedShapeIndex] : nil;
}

// shape (badge)
- (void)selectShapeAtIndex:(NSUInteger)index {
   if (index == 0) {
        // revoke all recoloring
       [self setShapeWithIndex:-1];
    } else {
        // recolor
        if ([self.shapesDataSource isFreeShapeAtIndex:index - 1]) {
            UIImage *shapeImage = [self setShapeWithIndex:index - 1];
            [self.vc checkRecoloringAvailability];
            [[AppManager sharedInstance] checkMainColorOfShape:shapeImage withName:self.currentShapeName];
        } else {
            [self.vc showIAPPopup];
        }
    }
}

- (UIImage*)setShapeWithIndex:(NSUInteger)index {
    self.selectedShapeIndex = index;
    self.shapesDataSource.currentIndex = index;
    
    self.selectedShapeColor = nil;

    UIImage *shapeImage;
    if (index == -1) {

        self.selectedShapeIndex = -1;
        
    } else {

        NSString *shapeName = self.currentShapeName;
        if (!shapeName) return nil;
        shapeImage = [UIImage imageNamed:shapeName];
    }
    
    self.currentShape = shapeImage;
    self.vc.shapeImageView.image = shapeImage;
    return shapeImage;
}

// color (for shape)
- (void)selectShapeColorAtIndex:(NSUInteger)index {
    if (index == 0) {
        // revoke all recoloring
        self.selectedShapeColor = nil;
        self.vc.shapeImageView.image = self.currentShape;
    } else {
        // recolor
        UIColor *color = self.shapeColorDataSource.colors[index - 1];
        self.selectedShapeColor = color;
        [self shapeColorsReplace];
    }
}

- (void)backgroundColorsReplace {
    if (!self.selectedBackgroundColor) return;
    UIImage *img = self.currentBackgroundPattern;
    UIColor *mainColor = [[AppManager sharedInstance] mainColorOfPattern:self.currentBackgroundPatternName];
    UIImage *image = [img imageInConvertedColors:mainColor intoColor:self.selectedBackgroundColor inContext:[AppManager sharedInstance].context];

    self.vc.backgroundImageView.backgroundColor = [UIColor colorWithPatternImage:image];
}

- (void)shapeColorsReplace {
    if (!self.selectedShapeColor) return;
    UIImage *img = self.currentShape;
    UIColor *mainColor = [[AppManager sharedInstance] mainColorOfShape:self.currentShapeName];
    UIImage *image = [img imageInConvertedColors:mainColor intoColor:self.selectedShapeColor inContext:[AppManager sharedInstance].context];
    self.vc.shapeImageView.image = image;
}

- (BOOL)onTextFieldChangeToString:(NSString*)newString byTypingString:(NSString*)string {
    // validation if new symbols were typed, not for erasing.
    if (string.length > 0) {
        // max length for Circle font
        if ([CircleMonogramsFont isOwnFontWithName:self.currentFontName] &&
            newString.length > CircleMonogramsFont.maxLength) {
            [self.vc showMessage:[NSString stringWithFormat:@"Length is limited by %d for selected font", CircleMonogramsFont.maxLength] withTitle:nil withAction:nil];
            return NO;
        }

        // supported glyphs for all fonts
        unichar charToValidate = [string characterAtIndex:0];
        if ([CircleMonogramsFont isOwnFontWithName:self.currentFontName] &&
            [string isEqualToString:@"&"]) {
             // Circle Font supports the "&" sign only on the middle place of 3-letter version (but it's under the "~" sign)
            if (newString.length != 2) {
                [self.vc showMessage:@"'&' sign may be used just as the 2nd letter of 3" withTitle:nil withAction:nil];
                return NO;
            }
        } else {
            if (![TextChecker isCharacter:charToValidate supportedByFont:self.vc.monogramLabel.font]) {
                [self.vc showMessage:@"This symbol isn't supported by selected font" withTitle:nil withAction:nil];
                return NO;
            }
        }

    
    // if string is nil - it's tapping the 'return' key - so no new letters are typed)
    } else if (string == nil) {
        // if user try to apply 2-letter string with 2nd sign '&' to the Circle font
        if ([CircleMonogramsFont isOwnFontWithName:self.currentFontName] &&
            newString.length == 2 &&
            [[newString substringFromIndex:1] isEqualToString:@"&"]) {
            [self.vc showMessage:@"'&' sign may be used just as the 2nd letter of 3" withTitle:nil withAction:nil];
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - save monogram to list of users' monograms / image to Photos
- (void)saveCreation {
    [self.vc hideAllBars];
    UIImage *monogramImage = [self.vc.view.layer getUIImage];
    UIImageWriteToSavedPhotosAlbum(monogramImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);

    [[AppManager sharedInstance] saveMonogramWithBackgroundCategoryIndex:self.selectedBackgroundCategoryIndex
                                                         backgroundIndex:self.selectedBackgroundIndex
                                                         backgroundColor:self.selectedBackgroundColor
                                                              shapeIndex:self.selectedShapeIndex
                                                              shapeColor:self.selectedShapeColor
                                                               fontIndex:self.selectedFontIndex
                                                                fontSize:self.vc.monogramLabel.font.pointSize
                                                               fontColor:self.vc.monogramLabel.textColor
                                                                    text:self.vc.monogramTextInput.text
                                                              finalImage:monogramImage
                                                              photoImage:self.vc.backgroundImageView.image];
    [self.vc hideMainToolbars:NO];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    if (error) {
        [self.vc showMessage:error.localizedDescription withTitle:@"Error" withAction:nil];
    } else {
        [self.vc showMessage:@"You can set this design as wallpaper from within the Photos app." withTitle:@"Saved to Camera Roll" withAction:nil];
    }
}

- (void)updateFontForSize:(CGFloat)size {
    [self correctMonogramLabelXPosition];
    self.vc.monogramLabel.font = [UIFont fontWithName:self.vc.monogramLabel.font.fontName size:size];
}

- (void)setFontSliderValueWithFontSize:(int)pointSize {
    CGFloat currentValue = (float)(pointSize - kMinFontSize) / (kMaxFontSize - kMinFontSize);
    self.vc.fontsizeSlider.value = currentValue;
}

#pragma mark - restore saved monogram
- (void)initCurrentValues {
    AppManager *am = [AppManager sharedInstance];
    
    if (am.currentMonogramNumber >= 0 && am.currentMonogramNumber < am.monogramsList.count) {
        [self setBackgroundCategoryWithIndex:am.savedBackgroundCategoryIndex.integerValue];

        NSInteger bgIndex = am.savedBackgroundIndex.integerValue;
        if (self.currentBackgroundCategoryIndex != am.savedBackgroundCategoryIndex.integerValue) {
            // it means that saved background category is currently unavailable
            bgIndex = -1;
        }
        UIImage *photoFromLibrary = am.savedUsedPhoto;
        if (photoFromLibrary) {
            [self setBackgroundPhoto:photoFromLibrary];
        } else {
            [self setBackgroundPatternWithIndex:bgIndex];
        }
    
        self.selectedBackgroundColor = am.savedBackgroundColor;
        [self backgroundColorsReplace];
        [self.vc checkRecoloringAvailability];
        
        [self setShapeWithIndex:am.savedShapeIndex.integerValue];
        
        self.selectedShapeColor = am.savedShapeColor;
        [self shapeColorsReplace];
        [self.vc checkRecoloringAvailability];
        
        [self setFontIndex:am.savedFontIndex.integerValue];
        [self setFontSliderValueWithFontSize:am.savedFontSize];
        [self updateFontForSize:am.savedFontSize];
        
        self.vc.monogramTextInput.text = am.savedMonogramText;
        [self setTextToMonogram];
        
        self.vc.monogramLabel.textColor = am.savedFontColor;
        
    } else {
        [self selectBackgroundCategoryAtIndex:0];
        [self setFontIndex:0];
        [self selectTextColorAtIndex:0];
        [self setTextToMonogram];
        [self setFontSliderValueWithFontSize:self.vc.monogramLabel.font.pointSize];
    }
}

#pragma mark - on purchase complete
- (void)onPurchaseComplete:(NSNotification*)notification {
    // get data from AppManager and update visible cells of collectionviews
    [self unlockPurchasedContent];
    [self.vc.backgroundCategoriesCollection reloadData];
    [self.vc.backgroundsCollection reloadData];
    [self.vc.shapesCollection reloadData];
    [self.vc.fontsCollection reloadData];
}

- (void)unlockPurchasedContent {
    for (Product *product in [AppManager sharedInstance].products.allValues) {
        if (product.isAvailable) {
            if([product.productIdentifier isEqualToString:kPurchaseID_Fonts]) {
                self.fontsDataSource.isUnlocked = YES;
            } else if([product.productIdentifier isEqualToString:kPurchaseID_Wallpapers]) {
                self.backgroundCategoriesDataSource.isUnlocked = YES;
                self.backgroundsDataSource.isUnlocked = YES;
            } else if([product.productIdentifier isEqualToString:kPurchaseID_Badges]) {
                self.shapesDataSource.isUnlocked = YES;
            } else if([product.productIdentifier isEqualToString:kPurchaseID_AdsFree]) {

# warning insert here your code - to indicate whether to show/hide ads
                
            } else if([product.productIdentifier isEqualToString:kPurchaseID_PRO]) {
                self.fontsDataSource.isUnlocked = YES;
                self.backgroundCategoriesDataSource.isUnlocked = YES;
                self.backgroundsDataSource.isUnlocked = YES;
                self.shapesDataSource.isUnlocked = YES;
            }
        }
    }

}

@end
