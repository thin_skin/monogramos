//
//  IAPPopupViewController.h
//  monogramos
//
//  Created by ElenaD on 4/14/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface IAPPopupViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *fontsTitle;
@property (weak, nonatomic) IBOutlet UILabel *fontsDescription;
@property (weak, nonatomic) IBOutlet UILabel *fontsPrice;
@property (weak, nonatomic) IBOutlet UILabel *wallpapersTitle;
@property (weak, nonatomic) IBOutlet UILabel *wallpapersDescription;
@property (weak, nonatomic) IBOutlet UILabel *wallpapersPrice;
@property (weak, nonatomic) IBOutlet UILabel *shapesTitle;
@property (weak, nonatomic) IBOutlet UILabel *shapesDescription;
@property (weak, nonatomic) IBOutlet UILabel *shapesPrice;
@property (weak, nonatomic) IBOutlet UILabel *removeAdsTitle;
@property (weak, nonatomic) IBOutlet UILabel *removeAdsDescription;
@property (weak, nonatomic) IBOutlet UILabel *removeAdsPrice;
@property (weak, nonatomic) IBOutlet UILabel *buyAllTitle;
@property (weak, nonatomic) IBOutlet UILabel *buyAllPrice;

@end
