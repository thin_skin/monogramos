//
//  BaseViewController.m
//  monogramos
//
//  Created by lenka on 4/8/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import "BaseViewController.h"
#import "const.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showError:) name:kErrorNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kErrorNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showError:(NSNotification*)notification {
    id obj;
    if (notification.userInfo && (obj = notification.userInfo.allValues[0])) {
        NSString *msg;
        if ([obj isKindOfClass:[NSString class]]) {
            msg = obj;
        } else if ([obj isKindOfClass:[NSError class]]) {
            msg = [obj localizedDescription];
        }
        [self showMessage:msg withTitle:@"Error" withAction:nil];
    }
}

- (void)showMessage:(NSString*_Nonnull)str withTitle:(NSString*_Nullable)title withAction:(UIAlertAction*)action {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:(title ? title : str)
                                                                         message:(title ? str : nil)
                                                                  preferredStyle:UIAlertControllerStyleAlert];
        if (action) {
            UIAlertAction *cancelAction =  [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:cancelAction];
            [alertVC addAction:action];
        } else {
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            [alertVC addAction:okAction];
        }
        [weakSelf presentViewController:alertVC animated:YES completion:nil];
    });
}

- (void)showShareMenu:(NSArray*_Nonnull)shareItems
           fromButton:(UIBarButtonItem*_Nonnull)barButton
           completion:(void (^ __nullable)(void))completion {
    UIActivityViewController *shareVC = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    shareVC.popoverPresentationController.barButtonItem = barButton;
    shareVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if (completion) completion();
    };
    [self presentViewController:shareVC animated:YES completion:nil];
}
@end
