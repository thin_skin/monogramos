//
//  AppManager.h
//  monogramos
//
//  Created by lenka on 3/18/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "const.h"

#import "StoreObserver.h"

#define kSavedBackgroundCategoryIndex   @"bgCatNumber"
#define kSavedBackgroundIndex           @"bgNumber"
#define kSavedBackground                @"bgName"
#define kSavedBackgroundColor           @"bgColor"
#define kSavedShapeIndex                     @"shapeIndex"
#define kSavedShape                     @"shapeName"
#define kSavedShapeColor                @"shapeColor"
#define kSavedText                      @"monogramText"
#define kSavedFontIndex                      @"fontIndex"
#define kSavedFont                      @"fontName"
#define kSavedFontSize                  @"fontSize"
#define kSavedFontColor                 @"fontColor"
#define kSavedMonogramText              @"monogramText"
#define kSavedImageFileName             @"filenameForPreview"
#define kSavedPhotoFileName             @"filenameForBackgroundPhoto"


@interface Product : NSObject <NSCoding>

@property (nonatomic) NSString *productIdentifier;
@property (nonatomic) NSDecimalNumber *price;
@property (nonatomic) NSString *localizedPrice;
@property (nonatomic) NSString *localizedTitle;
@property (nonatomic) NSString *localizedDescription;
@property BOOL isAvailable;

- (instancetype)initWithIdentifier:(NSString*)identifier
                         withTitle:(NSString*)title
                   withDescription:(NSString*)description
                         withPrice:(NSDecimalNumber*)price
                withLocalizedPrice:(NSString*)localizedPrice;

@end;

@interface AppManager : NSObject <StoreObserverProtocol>
{
    StoreObserver           *observer;
}

@property dispatch_queue_t image_processing_queue;
@property CIContext *context;

@property NSArray *categories;
@property NSMutableDictionary *paidContentData;

@property NSMutableDictionary<NSString *, NSData *> *wallpaperMainColors;
@property NSMutableDictionary<NSString *, NSData *> *shapeMainColors;

@property NSMutableArray *monogramsList;
@property NSInteger currentMonogramNumber;

@property NSDictionary<NSString *, Product *> *products;
@property NSMutableDictionary *SKProductsById;

+ (instancetype)sharedInstance;

// IAP
- (void) requestPrices;
- (void) purchase:(NSString *)purchase_id;
- (void) cancelLoadingAlert;
- (void) restorePurchases;

// asynchronous processing of image
- (void)checkMainColorOfPattern:(UIImage*)img withName:(NSString*)patternFilename;
- (void)checkMainColorOfShape:(UIImage*)img withName:(NSString*)shapeFilename;

- (id)mainColorOfPattern:(NSString*)patternFilename;
- (id)mainColorOfShape:(NSString*)shapeFilename;

// save design
- (void)saveMonogramWithBackgroundCategoryIndex:(NSInteger)backgroundCategoryIndex
                                backgroundIndex:(NSInteger)backgroundIndex
                                backgroundColor:(UIColor*)backgroundColor
                                     shapeIndex:(NSInteger)shapeIndex
                                     shapeColor:(UIColor*)shapeColor
                                      fontIndex:(NSInteger)fontIndex
                                       fontSize:(float)fontSize
                                      fontColor:(UIColor*)fontColor
                                           text:(NSString*)monogramText
                                     finalImage:(UIImage*)finalImage
                                     photoImage:(UIImage*)photoImage;

// edit designs list
- (void)deleteCurrentMonogram;
- (void)deleteMonogramsAtPositions:(NSArray*)posArray;

- (UIImage*)previewForMonogramAtPosition:(NSUInteger)index;
- (UIImage*)photoForMonogramAtPosition:(NSUInteger)index;

// access saved data for currentMonogramNumber
- (NSString*)savedBackgroundCategoryIndex;
- (NSString*)savedBackgroundIndex;
- (UIImage*)savedUsedPhoto;
- (UIColor*)savedBackgroundColor;
- (NSString*)savedShapeIndex;
- (UIColor*)savedShapeColor;
- (NSString*)savedFontIndex;
- (float)savedFontSize;
- (UIColor*)savedFontColor;
- (NSString*)savedMonogramText;

@end
