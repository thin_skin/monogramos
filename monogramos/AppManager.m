//
//  AppManager.m
//  monogramos
//
//  Created by lenka on 3/18/17.
//  Copyright © 2017 prolev. All rights reserved.
//

@import UIKit;
#import "AppManager.h"
#import "UIImage+ReplaceColor.h"
#import "MBProgressHUD.h"

#define kWallpaperMainColorsKey  @"MonogramDataWallpaperMainColors"
#define kShapeMainColorsKey  @"MonogramDataShapeMainColors"
#define kUserMonogramListKey  @"MonogramDataUserCreationsList"
#define kInAppProducts @"MonogramDataUsersColors"

#define kPreviewFilePrefix  @"preview_monogram_"
#define kPhotoFilePrefix  @"photo_from_library_"
#define DocumentsDirectoryPath      [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

@implementation Product

- (instancetype)initWithIdentifier:(NSString *)identifier withTitle:(NSString *)title withDescription:(NSString *)description withPrice:(NSDecimalNumber *)price withLocalizedPrice:(NSString *)localizedPrice {
    if (self = [super init]) {
        _productIdentifier = identifier;
        _localizedTitle = title;
        _localizedDescription = description;
        _price = price;
        _localizedPrice = localizedPrice;
    }
    return self;
}

#define kProductID                      @"_productIdentifier"
#define kProductPrice                   @"_price"
#define kProductLocalizedTitle          @"_localizedTitle"
#define kProductLocalizedDescription    @"_localizedDescription"
#define kProductLocalizedPrice          @"_localizedPrice"
#define kProductIsAvailable             @"_isAvailable"

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_productIdentifier forKey:kProductID];
    [aCoder encodeObject:_price forKey:kProductPrice];
    [aCoder encodeObject:_localizedTitle forKey:kProductLocalizedTitle];
    [aCoder encodeObject:_localizedPrice forKey:kProductLocalizedPrice];
    [aCoder encodeObject:_localizedDescription forKey:kProductLocalizedDescription];
    [aCoder encodeBool:_isAvailable forKey:kProductIsAvailable];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        _productIdentifier = [aDecoder decodeObjectForKey:kProductID];
        _price = [aDecoder decodeObjectForKey:kProductPrice];
        _localizedTitle = [aDecoder decodeObjectForKey:kProductLocalizedTitle];
        _localizedDescription = [aDecoder decodeObjectForKey:kProductLocalizedDescription];
        _localizedPrice = [aDecoder decodeObjectForKey:kProductLocalizedPrice];
        _isAvailable = [aDecoder decodeBoolForKey:kProductIsAvailable];
    }
    return self;
}

@end


@implementation AppManager
+ (instancetype)sharedInstance {
    static AppManager *sharedInstance = nil;
    static dispatch_once_t onceToken; // onceToken = 0
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
        // data from Content.plist
        NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"Content" ofType:@"plist"];
        NSDictionary *content = [NSDictionary dictionaryWithContentsOfFile:plistPath];
        self.categories = content[kContentPlistCategoriesKey];
        self.paidContentData = content[kContentPlistPaidDataKey];
        
        // cached main colors, calculated before
        self.wallpaperMainColors = [[[NSUserDefaults standardUserDefaults] objectForKey:kWallpaperMainColorsKey] mutableCopy];
        if (!self.wallpaperMainColors) self.wallpaperMainColors = [@{} mutableCopy];
        self.shapeMainColors = [[[NSUserDefaults standardUserDefaults] objectForKey:kShapeMainColorsKey] mutableCopy];
        if (!self.shapeMainColors) self.shapeMainColors = [@{} mutableCopy];
        
        // saved designs
        self.monogramsList = [[[NSUserDefaults standardUserDefaults] objectForKey:kUserMonogramListKey] mutableCopy];
        if (!self.monogramsList) self.monogramsList = [@[] mutableCopy];
        
        // image processing and CoreImage context
        self.image_processing_queue = dispatch_queue_create("com.prolev.Monogramos.imageColorCalculation", DISPATCH_QUEUE_SERIAL);
        self.context = [CIContext context];
        
        // IAP
        [self loadProducts];
        self.SKProductsById = [@{} mutableCopy];
        //Store Observer
        observer = [[StoreObserver alloc] init];
        observer.delegate = self;
        [[SKPaymentQueue defaultQueue] addTransactionObserver:observer];
        [self requestPrices];
    }
    return self;
}

- (void)checkMainColorOfPattern:(UIImage*)img withName:(NSString*)patternFilename {
    if (!img) return;
    if (![self mainColorOfPattern:patternFilename]) {
        __weak typeof(self) weakSelf = self;
        [self startSearchingColorsOfImage:img withName:patternFilename withCompletion:^(UIColor *mainColor){
            [weakSelf saveMainColor:mainColor ofPattern:patternFilename];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMainColorsUpdate object:nil];
        }];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMainColorsUpdate object:nil];
    }
}

- (void)checkMainColorOfShape:(UIImage*)img withName:(NSString*)shapeFilename {
    if (!img) return;
    if (![self mainColorOfShape:shapeFilename]) {
        __weak typeof(self) weakSelf = self;
        [self startSearchingColorsOfImage:img withName:shapeFilename withCompletion:^(UIColor *mainColor) {
            [weakSelf saveMainColor:mainColor ofShape:shapeFilename];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMainColorsUpdate object:nil];
        }];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMainColorsUpdate object:nil];
    }
}


- (void)startSearchingColorsOfImage:(UIImage*)img withName:(NSString*)filename withCompletion:(void (^ __nullable) (UIColor* mainColor))completionBlock {
    dispatch_async(self.image_processing_queue, ^{
        @synchronized (self) {
            NSMutableDictionary *colors = [img mainColoursInImageInDetail:1]; // 0 - low, 1 - default, 2 - high
            NSNumber *maxRate = [colors.allValues valueForKeyPath:@"@max.self"];
            id mainColor = [[colors allKeysForObject:maxRate] firstObject];
            CGFloat h,s,v;
            [mainColor getHue:&h saturation:&s brightness:&v alpha:nil];
            if (maxRate.floatValue < 0.5) {
                mainColor = kNotRecolorableColor;
            } else {
                while (colors.count > 0 &&
                       ((h == 0 && s == 0 && v == 1) ||  // exclude whitish tones
                        (s < 0.1 && v > 0.9))) { // and transparency in png
                    [colors removeObjectForKey:mainColor];
                    if (colors.count == 0) {
                        mainColor = kNotRecolorableColor;
                    } else {
                        // if we have another colors in list
//                        float remainedPercent = 1 - maxRate.floatValue;
                        maxRate = [colors.allValues valueForKeyPath:@"@max.self"];
                        mainColor = [[colors allKeysForObject:maxRate] firstObject];
                        [mainColor getHue:&h saturation:&s brightness:&v alpha:nil];
//                        if (maxRate.floatValue < remainedPercent / 2) {
//                            mainColor = kNotRecolorableColor;
//                        }
                    }
                }
            }
            completionBlock(mainColor);
        }
    });

}
// data for recoloring of wallpapers
- (id)mainColorOfPattern:(NSString*)patternFilename {
    id obj = self.wallpaperMainColors[patternFilename];
    if ([obj isKindOfClass:[NSData class]])
        return [NSKeyedUnarchiver unarchiveObjectWithData:obj];
    else
        return obj; // if it's nil
}

- (void)saveMainColor:(UIColor*)color ofPattern:(NSString*)patternFilename {
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
    [self.wallpaperMainColors setObject:colorData forKey:patternFilename];
    [[NSUserDefaults standardUserDefaults] setObject:self.wallpaperMainColors forKey:kWallpaperMainColorsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// data for recoloring of shapes
- (id)mainColorOfShape:(NSString*)shapeFilename {
    id obj = self.shapeMainColors[shapeFilename];
    if ([obj isKindOfClass:[NSData class]])
        return [NSKeyedUnarchiver unarchiveObjectWithData:obj];
    else
        return obj;
}

- (void)saveMainColor:(UIColor*)color ofShape:(NSString*)shapeFilename {
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
    [self.shapeMainColors setObject:colorData forKey:shapeFilename];
    [[NSUserDefaults standardUserDefaults] setObject:self.shapeMainColors forKey:kShapeMainColorsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - save all data and preview for the monogram
- (NSString*)pathForFilename:(NSString*)filename {
    return [DocumentsDirectoryPath stringByAppendingPathComponent:filename];
}

- (NSString*)filenameForSavedImage:(UIImage*)image WithPrefix:(NSString*)filenamePrefix {
    if (!image) return @"";
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    NSString *filename = [NSString stringWithFormat:@"%@%@.jpg", filenamePrefix, [NSUUID UUID].UUIDString];
    NSString *imagePath = [self pathForFilename:filename];
    
    dispatch_async([AppManager sharedInstance].image_processing_queue, ^{
        // Write image data to user's folder
        [imageData writeToFile:imagePath atomically:YES];
        if ([filenamePrefix isEqualToString:kPreviewFilePrefix]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMonogramPreviewUpdate object:nil];
        }
    });
    
    return filename;
}

- (UIImage*)imageFromPath:(NSString*)imagePath {
    if (imagePath) {
        return [UIImage imageWithData:[NSData dataWithContentsOfFile:imagePath]];
    }
    return nil;
}

- (void)deleteImageWithPath:(NSString*)imagePath {
    if (imagePath && imagePath.length > 0) {
        dispatch_async(self.image_processing_queue, ^{
            NSError *err;
            [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&err];
        });
    }
}

- (UIImage*)previewForMonogramAtPosition:(NSUInteger)index {
    NSString *imageFilename = [self.monogramsList[index] objectForKey:kSavedImageFileName];
    return [self imageFromPath:[self pathForFilename:imageFilename]];
}

- (UIImage*)photoForMonogramAtPosition:(NSUInteger)index {
    NSString *imageFilename = [self.monogramsList[index] objectForKey:kSavedPhotoFileName];
    return [self imageFromPath:[self pathForFilename:imageFilename]];
}

- (void)saveMonogramWithBackgroundCategoryIndex:(NSInteger)backgroundCategoryIndex
                                backgroundIndex:(NSInteger)backgroundIndex
                                backgroundColor:(UIColor*)backgroundColor
                                     shapeIndex:(NSInteger)shapeIndex
                                     shapeColor:(UIColor*)shapeColor
                                      fontIndex:(NSInteger)fontIndex
                                       fontSize:(float)fontSize
                                      fontColor:(UIColor*)fontColor
                                           text:(NSString*)monogramText
                                     finalImage:(UIImage*)finalImage
                                     photoImage:(UIImage*)photoImage {

    if (self.currentMonogramNumber < 0) {
        self.currentMonogramNumber = self.monogramsList.count;
    }
    
    NSString *filenameForPreviewImage = [self filenameForSavedImage:finalImage WithPrefix:kPreviewFilePrefix];
    NSString *filenameForUsedPhoto = [self filenameForSavedImage:photoImage WithPrefix:kPhotoFilePrefix];
    
    NSMutableDictionary *allParameters = [@{kSavedBackgroundCategoryIndex: @(backgroundCategoryIndex),
                                            kSavedBackgroundIndex: @(backgroundIndex),
                                            kSavedShapeIndex: @(shapeIndex),
                                            kSavedFontIndex: @(fontIndex),
                                            kSavedFontSize: @(fontSize),
                                            kSavedFontColor: [NSKeyedArchiver archivedDataWithRootObject:fontColor],
                                            kSavedMonogramText: monogramText,
                                            kSavedImageFileName: filenameForPreviewImage,
                                            kSavedPhotoFileName: filenameForUsedPhoto} mutableCopy];
    
    
    if (backgroundColor) {
        [allParameters setObject:[NSKeyedArchiver archivedDataWithRootObject:backgroundColor] forKey:kSavedBackgroundColor];
    }
    if (shapeColor) {
        [allParameters setObject:[NSKeyedArchiver archivedDataWithRootObject:shapeColor] forKey:kSavedShapeColor];
    }
    
    if (self.monogramsList.count > self.currentMonogramNumber) {
        [self.monogramsList replaceObjectAtIndex:self.currentMonogramNumber withObject:allParameters];
    } else {
        [self.monogramsList addObject:allParameters];
    }
    [self updateMonogramsList];
}


- (void)deleteCurrentMonogram {
    [self deleteMonogramsAtPositions:@[@(_currentMonogramNumber)]];
}

- (void)deleteMonogramImagesAtPosition:(NSInteger)num {
    NSString *pathToPreview = self.monogramsList[num][kSavedImageFileName];
    [self deleteImageWithPath:pathToPreview];
    NSString *pathToUsedPhoto = self.monogramsList[num][kSavedPhotoFileName];
    [self deleteImageWithPath:pathToUsedPhoto];
}

- (void)deleteMonogramsAtPositions:(NSArray*)posArray {

    NSMutableIndexSet *set = [NSMutableIndexSet indexSet];
    for (NSNumber *num in posArray) {
        [self deleteMonogramImagesAtPosition:num.integerValue];
        [set addIndex:num.integerValue];
    }
    [self.monogramsList removeObjectsAtIndexes:set];
    [self updateMonogramsList];
}

- (void) updateMonogramsList {
    [[NSUserDefaults standardUserDefaults] setObject:self.monogramsList forKey:kUserMonogramListKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMonogramListUpdate object:nil];
}

#pragma mark - access to saved data
- (NSNumber*)savedBackgroundCategoryIndex {
    return _monogramsList[_currentMonogramNumber][kSavedBackgroundCategoryIndex];
}

- (NSNumber*)savedBackgroundIndex {
    return _monogramsList[_currentMonogramNumber][kSavedBackgroundIndex];
}

- (UIColor*)savedBackgroundColor {
    NSData *data = _monogramsList[_currentMonogramNumber][kSavedBackgroundColor];
    return data ? [NSKeyedUnarchiver unarchiveObjectWithData:data] : nil;
}

- (NSString*)savedShapeIndex {
    return _monogramsList[_currentMonogramNumber][kSavedShapeIndex];
}

- (UIColor*)savedShapeColor {
    NSData *data = _monogramsList[_currentMonogramNumber][kSavedShapeColor];
    return data ? [NSKeyedUnarchiver unarchiveObjectWithData:data] : nil;
}

- (NSString*)savedFontIndex {
    return _monogramsList[_currentMonogramNumber][kSavedFontIndex];
}

- (float)savedFontSize {
    return [_monogramsList[_currentMonogramNumber][kSavedFontSize] floatValue];
}

- (UIColor*)savedFontColor {
    NSData *data = _monogramsList[_currentMonogramNumber][kSavedFontColor];
    return data ? [NSKeyedUnarchiver unarchiveObjectWithData:data] : nil;
}

- (NSString*)savedMonogramText {
    return _monogramsList[_currentMonogramNumber][kSavedMonogramText];
}

- (UIImage*)savedUsedPhoto {
    return [self photoForMonogramAtPosition:_currentMonogramNumber];
}

#pragma mark - IAP
- (void)saveProducts {
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:self.products]
                                              forKey:kInAppProducts];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadProducts {
    self.products = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:kInAppProducts]];
}

- (void) requestPrices {
    NSSet *productIdSet = [NSSet setWithObjects:
                           kPurchaseID_Wallpapers,
                           kPurchaseID_Badges,
                           kPurchaseID_Fonts,
                           kPurchaseID_AdsFree,
                           kPurchaseID_PRO,
                           nil];
    
    [observer requestProductDataForSet:productIdSet];
}

#pragma mark - InAppPurchases - StoreObserverProtocol
- (void) transactionDidError:(NSError*)error {
    [self cancelLoadingAlert];

    if(error != nil && error.code != SKErrorPaymentCancelled) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kErrorNotification
                                                            object:nil userInfo:@{@"message" : [error localizedDescription]}];
    }
}

- (void) transactionDidFinish:(NSString*)productIdentifier {
    [self cancelLoadingAlert];
    if (productIdentifier) {
        [self completePurchaseForProductId:productIdentifier];
    }
}

- (void)completePurchaseForProductId:(NSString*)productId {
    Product* item = [self.products objectForKey:productId];
    item.isAvailable = YES;
    [self saveProducts];
}

- (void)didReceiveResponseWithProducts:(NSArray<SKProduct *> *)products {
    NSMutableDictionary<NSString *, Product *> *dic = [@{} mutableCopy];
    for (SKProduct *prod in products) {

        [self.SKProductsById setObject:prod forKey:prod.productIdentifier];
        
        Product *localProduct = self.products[prod.productIdentifier];
        if (localProduct) {
            localProduct.localizedTitle = prod.localizedTitle;
            localProduct.price = prod.price;
            localProduct.localizedPrice = prod.localizedPrice;
            localProduct.localizedDescription = prod.localizedDescription;
        } else {
            localProduct = [[Product alloc] initWithIdentifier:prod.productIdentifier
                                                     withTitle:prod.localizedTitle
                                               withDescription:prod.localizedDescription
                                                     withPrice:prod.price
                                            withLocalizedPrice:prod.localizedPrice];
        }
        [dic setObject:localProduct forKey:prod.productIdentifier];
    }
    self.products = dic;
    [self saveProducts];

}

#pragma mark - purchase and restore
- (void)purchase:(NSString *)purchase_id {
    if (![SKPaymentQueue canMakePayments]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kErrorNotification
                                                            object:nil userInfo:@{@"messsage" : @"in-App purchases are disabled"}];
        return;
    }
    
    SKProduct *product = [self.SKProductsById objectForKey:purchase_id];
    if(product != nil) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        [self showWaitingAlert];
    }
}

- (void) restorePurchases {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    [self showWaitingAlert];
}


#pragma mark - loading HUD 
- (UIView*)visibleView {
    UIViewController* rootVC = [UIApplication sharedApplication].delegate.window.rootViewController;
    return [rootVC isKindOfClass:[UINavigationController class]] ?
                [(UINavigationController*)rootVC visibleViewController].view :
                                                                 rootVC.view;
}

- (void) showWaitingAlert {
    [MBProgressHUD showHUDAddedTo:[self visibleView] animated:YES];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"[IN APP PURCHASE] request error: %@", [error localizedDescription]);
}

- (void) transactionDidError {
    [MBProgressHUD hideHUDForView:[self visibleView] animated:YES];
}

- (void) cancelLoadingAlert {
    [MBProgressHUD hideHUDForView:[self visibleView] animated:YES];
}


@end
