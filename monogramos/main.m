//
//  main.m
//  monogramos
//
//  Created by lenka on 2/25/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
