//
//  AppDelegate.h
//  monogramos
//
//  Created by lenka on 2/25/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;

@end

