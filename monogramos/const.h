//
//  const.h
//  monogramos
//
//  Created by lenka on 3/20/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#ifndef const_h
#define const_h

#define IS_IPAD    (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
#define IS_IPAD_PRO (MAX([[UIScreen mainScreen]bounds].size.width,[[UIScreen mainScreen] bounds].size.height) > 1024)

#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

#define kMinFontSize    10
#define kMaxFontSize    (IS_IPAD ? 440 : 220)

#define kBorderColorForSelectedItem [UIColor colorWithRed:0.27 green:0.373 blue:0.424 alpha:1]


#define    kWallpapersCollectionRemoveIndex         0
#define    kWallpapersCollectionAddPhotoIndex       1

#define kNotificationMainColorsUpdate     @"kNotificationMainColorsUpdate"
#define kNotificationMonogramListUpdate   @"kNotificationMonogramListUpdate"
#define kNotificationMonogramPreviewUpdate   @"kNotificationMonogramPreviewUpdate"


#define kNotRecolorableColor            @"NotRecolorableWallpaperColor"

#define kContentPlistCategoriesKey              @"Categories"
#define kContentPlistCategoryPatternKey         @"patternName"
#define kContentPlistCategoryDisplayNameKey     @"displayName"
#define kContentPlistCategoryFreeCountKey       @"freeCount"
#define kContentPlistCategoryMaxIndexKey        @"maxIndex"
#define kContentPlistPaidDataKey                @"paidData"
#define kContentPlistPaidDataBadgesKey          @"Badges"
#define kContentPlistPaidDataFontsKey           @"Fonts"
#define kContentPlistPaidDataFreeCount          @"freeCount"


// purchases
// for Bundle ID : com.7april2017.5pm
#define kPurchaseID_Fonts       @"com.7april2017.5pm.fonts"
#define kPurchaseID_Badges       @"com.7april2017.5pm.badges"
#define kPurchaseID_Wallpapers       @"com.7april2017.5pm.wallpapers"
#define kPurchaseID_AdsFree       @"com.7april2017.5pm.removeads"
#define kPurchaseID_PRO         @"com.7april2017.5pm.PRO"

// for Bundle ID : com.prolev.Monogramus
//#define kPurchaseID_Fonts       @"com.prolev.Monogramus.fonts"
//#define kPurchaseID_Badges       @"com.prolev.Monogramus.badges"
//#define kPurchaseID_Wallpapers       @"com.prolev.Monogramus.wallpapers"
//#define kPurchaseID_AdsFree       @"com.prolev.Monogramus.removeads"
//#define kPurchaseID_PRO         @"com.prolev.Monogramus.PRO"

#define kPurchaseCompleteNotification @"kPurchaseCompleteNotification"
#define kUpdatePriceNotification        @"kUpdatePriceNotification"

#define kErrorNotification @"kErrorNotification"
#define kPurchaseTransactionErrorNotification @"kPurchaseTransactionErrorNotification"


#endif /* const_h */
