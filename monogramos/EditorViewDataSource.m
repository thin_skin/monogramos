//
//  EditorViewDataSource.m
//  monogramos
//
//  Created by lenka on 3/13/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import "EditorViewDataSource.h"
#import "CircleMonogramsFont.h"
#import "const.h"

#pragma mark - custom UICollectionViewCells
@implementation FontsCollectionCell
- (void)prepareForReuse {
    self.layer.borderWidth = 0;
    self.layer.borderColor = [UIColor clearColor].CGColor;
}
@end

@implementation BackgroundsCollectionCell
- (void)prepareForReuse {
    self.layer.borderWidth = 0;
    self.layer.borderColor = [UIColor clearColor].CGColor;
}
@end

@implementation BackgroundCategoriesCollectionCell
- (void)prepareForReuse {
    self.layer.borderWidth = 0;
    self.layer.borderColor = [UIColor clearColor].CGColor;
}
@end

@implementation BgColorCollectionCell

- (void)prepareForReuse {
    self.whiteCorner.hidden = NO;
    self.layer.borderWidth = 0;
    self.layer.borderColor = [UIColor clearColor].CGColor;
}

@end

@implementation ShapesCollectionCell
- (void)prepareForReuse {
    self.layer.borderWidth = 0;
    self.layer.borderColor = [UIColor clearColor].CGColor;
}
@end

#pragma mark - all the UICollectionViews dataSource methods

#pragma mark - color palettes of all types
@implementation ColorsCollectionDataSource

- (instancetype)init {
    if (self = [super init]) {
        
        self.colors = [NSMutableArray new];
        
        [self.colors addObject:[UIColor blackColor]];
        [self.colors addObject:[UIColor whiteColor]];

        for (float hue = 0.8f; hue < 1 || ((float)(((int)(hue * 100)) % 100)) / 100 < 0.8f; hue += 0.045) {
            [self.colors addObject:[UIColor colorWithHue:hue saturation:0.8 brightness:0.6 alpha:1]];
            [self.colors addObject:[UIColor colorWithHue:hue saturation:0.95 brightness:0.9 alpha:1]];
            [self.colors addObject:[UIColor colorWithHue:hue saturation:0.65 brightness:1 alpha:1]];
            [self.colors addObject:[UIColor colorWithHue:hue saturation:0.5 brightness:1 alpha:1]];
        }

        for (float hue = 0.8f; hue < 1 || ((float)(((int)(hue * 100)) % 100)) / 100 < 0.8f; hue += 0.15) {
            [self.colors addObject:[UIColor colorWithHue:hue saturation:1 brightness:0.4 alpha:1]];
        }
        
        [self.colors addObject:[UIColor blackColor]];
        
    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.colors.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ColorsCollectionCell" forIndexPath:indexPath];
    cell.backgroundColor = self.colors[indexPath.item];
    return cell;
}

@end

@implementation BackgroundColorsCollectionDataSource

- (instancetype)init {
    if (self = [super init]) {
        [self.colors removeObject:[UIColor whiteColor]]; // we don't need white color for background recoloring
    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.colors.count + 1; // add one for 'revoke any recoloring'
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BgColorCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BgColorCollectionCell" forIndexPath:indexPath];
    
    if (indexPath.item == 0) {
        cell.whiteCorner.hidden = YES;
        cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cancel_s.png"]];
    } else {
        cell.backgroundColor = self.colors[indexPath.item - 1];
    }
    return cell;
}

@end

@implementation ShapeColorsCollectionDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BgColorCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShapeColorCollectionCell" forIndexPath:indexPath];
    
    if (indexPath.item == 0) {
        cell.whiteCorner.hidden = YES;
        cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cancel_s.png"]];
    } else {
        cell.backgroundColor = self.colors[indexPath.item - 1];
    }
    return cell;
}



@end

#pragma mark - wallpapers
@implementation BackgroundCategoriesCollectionDataSource

- (instancetype)initWithCategories:(NSArray*)categoriesArray {
    if (self = [super init]) {
        self.categories = categoriesArray;
    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.categories.count;
}

- (BOOL)isFreeCategoryAtNumber:(NSUInteger)ind {
    if (self.isUnlocked) return YES;
    NSNumber *freeCount = self.categories[ind][kContentPlistCategoryFreeCountKey];
    return (freeCount.integerValue > 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BackgroundCategoriesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BackgroundCategoriesCollectionCell" forIndexPath:indexPath];
    
    NSUInteger ind = indexPath.item;
    cell.catName.text = self.categories[ind][kContentPlistCategoryDisplayNameKey];
    NSString *patternName = self.categories[ind][kContentPlistCategoryPatternKey];
    NSNumber *maxIndex = self.categories[ind][kContentPlistCategoryMaxIndexKey];
    UIImage *firstPattern;
    int i = 1;
    // if somehow the numeration isn't started from 1..
    do {
        firstPattern = [UIImage imageNamed:[NSString stringWithFormat:@"%@_%d.png", patternName, i]];
        i++;
    } while (!firstPattern && i <= maxIndex.integerValue);
    
    cell.catImage.image = firstPattern;
    
    cell.paidSign.hidden = [self isFreeCategoryAtNumber:ind];
    
    if (ind == self.currentIndex) {
        cell.layer.borderWidth = 2;
        cell.layer.borderColor = kBorderColorForSelectedItem.CGColor;
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(64, 48);
}

@end

@implementation BackgroundsCollectionDataSource

- (void)setCategoryName:(NSString*)categoryName withMaxIndex:(NSNumber*)maxIndex withFreeCount:(NSNumber*)freeCount{
    self.patterns = [@[] mutableCopy];
    self.isForSelectedCategory = NO;
    UIImage *img;
    for (int i = 1; i <= maxIndex.integerValue; i++) {
        NSString *wallpaperName = [NSString stringWithFormat:@"%@_%d.png", categoryName, i];
        img = [UIImage imageNamed:wallpaperName];
        if (img) {
            [self.patterns addObject:wallpaperName];
        }
    }
    self.freeCount = freeCount.integerValue;
}

- (BOOL)isFreePatternAtIndex:(NSUInteger)index {
    if (self.isUnlocked) return YES;
    return (index <= self.freeCount);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.patterns.count + 1 + 1; // one for 'revoke any bg', one for 'load from photos'
}

// index in array of wallpapers (excluding items with cross and PhotoLibrary item)
- (NSInteger)realPatternIndexForIndex:(NSUInteger)index {
    NSInteger patternIndex = index +
            (index > kWallpapersCollectionRemoveIndex ? -1 : 0) +
            (index > kWallpapersCollectionAddPhotoIndex ? -1 : 0);
    return patternIndex;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BackgroundsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BackgroundsCollectionCell" forIndexPath:indexPath];
    NSUInteger index = indexPath.item;
    cell.paidSign.hidden = YES;

    if (index == kWallpapersCollectionAddPhotoIndex){
        cell.backgroundDesign.image = [UIImage imageNamed:@"add_image.png"];
    } else if (index == kWallpapersCollectionRemoveIndex) {
        cell.backgroundDesign.image = [UIImage imageNamed:@"cancel_l.png"];
    } else {
        NSUInteger ind = [self realPatternIndexForIndex:index];
        cell.backgroundDesign.image = [UIImage imageNamed:self.patterns[ind]];
        if (self.isForSelectedCategory && ind == self.currentIndex) {
            cell.layer.borderWidth = 2;
            cell.layer.borderColor = kBorderColorForSelectedItem.CGColor;
        }
        cell.paidSign.hidden = [self isFreePatternAtIndex:ind];
    }
    
    return cell;
}

@end

#pragma mark - badges
@implementation ShapesCollectionDataSource

- (instancetype)init {
    if (self = [super init]) {
        self.shapes = [NSMutableArray new];
        for (int i = 0; i < 20; i++)
            [self.shapes addObject:[NSString stringWithFormat:@"Independence_Day_%d%@.png", i + 1, IS_IPAD ? @"@3x" : @""]];
        
        for (int i = 0; i < 50; i++)
            [self.shapes addObject:[NSString stringWithFormat:@"Water_%d%@.png", i + 1, IS_IPAD ? @"@3x" : @""]];
        
    }
    
    return self;
}

- (BOOL)isFreeShapeAtIndex:(NSUInteger)index {
    if (self.isUnlocked) return YES;
    return (index <= self.freeCount);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.shapes.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ShapesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShapesCollectionCell" forIndexPath:indexPath];
    
    if (indexPath.item == 0) {
        cell.shapeImage.image = [UIImage imageNamed:@"cancel_l.png"];
        cell.paidSign.hidden = YES;
    } else {
        NSUInteger ind = indexPath.item - 1;
        cell.shapeImage.image = [UIImage imageNamed:self.shapes[ind]];
        cell.paidSign.hidden = [self isFreeShapeAtIndex:ind];

        if (ind == self.currentIndex) {
            cell.layer.borderWidth = 2;
            cell.layer.borderColor = kBorderColorForSelectedItem.CGColor;
        }
    }
    
    
    return cell;
}


@end

#pragma mark - fonts
@implementation FontsCollectionDataSource

- (instancetype)init {
    if (self = [super init]) {
        self.fonts = [NSMutableArray new];
        NSMutableArray *circleFonts = [NSMutableArray new];
        BOOL isCircleFontsIncluded = NO;
        
        /*
         For Circle Monogram fonts - we need encoded fontname, which includes
         the font type (White of Black)
         and frame type (there Frame0 stands for 'No Frame')
         */
        for (int type = kBackgroundTypeWhite; type < kBackgroundTypeCount; type++) {
            for (int frame = 0; frame < kFrameTypeCount; frame++) {
                [circleFonts addObject:[CircleMonogramsFont encodedFontnameWithType:type andFrame:frame]];
            }
        }
        
        // reading "Fonts provided by application" from Info.plist
        NSMutableArray *fontFiles = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"UIAppFonts"];
        
        // For all the fonts (except Circle Monogram) - place correct font name to array
        for (NSString *fontFile in fontFiles) {
            NSURL *url = [[NSBundle mainBundle] URLForResource:fontFile withExtension:NULL];
            if (url != nil) {
                NSData *fontData = [NSData dataWithContentsOfURL:url];
                CGDataProviderRef fontDataProvider = CGDataProviderCreateWithCFData((__bridge CFDataRef)fontData);
                CGFontRef loadedFont = CGFontCreateWithDataProvider(fontDataProvider);
                NSString *postscriptName = CFBridgingRelease(CGFontCopyPostScriptName(loadedFont));
                CGFontRelease(loadedFont);
                CGDataProviderRelease(fontDataProvider);
                
                if ([CircleMonogramsFont isOwnFontWithName:postscriptName]) {
                    // insert prepared list of available Circle Fonts at the appropriate position
                    if (!isCircleFontsIncluded) {
                        [self.fonts addObjectsFromArray:circleFonts];
                        isCircleFontsIncluded = YES;
                    }
                } else {
                    [self.fonts addObject:postscriptName];
                }
            }
        }
    }
    return self;
}

- (BOOL)isFreeFontAtIndex:(NSUInteger)index {
    if (self.isUnlocked) return YES;
    return (index <= self.freeCount);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.fonts.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FontsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FontsCollectionCell" forIndexPath:indexPath];
    
    
    if (indexPath.item == 0) {
        cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cancel_l.png"]];
        cell.modelLabel.text = nil;
        cell.paidSign.hidden = YES;
    } else {
        NSUInteger ind = indexPath.item - 1;
        NSString *buttonText = @"A";
        NSString *selectedFontname = self.fonts[ind];
        NSString *buttonFontname = selectedFontname;
        
        cell.backgroundColor = [UIColor colorWithRed:0.674 green:0.874 blue:0.796 alpha:1];
        if ([CircleMonogramsFont isOwnFontWithName:buttonFontname]) {
            buttonText = @"aB";
            
            kBackgroundType type = [CircleMonogramsFont typeFromEncodedFontname:selectedFontname];
            int frameType = [CircleMonogramsFont frameTypeFromEncodedFontname:selectedFontname];
            buttonFontname = [CircleMonogramsFont fontnameForString:buttonText withBackgroundType:type];
            buttonText = [CircleMonogramsFont prepareString:buttonText withFrame:frameType];
        }
        
        cell.modelLabel.text = buttonText;
        cell.modelLabel.font = [UIFont fontWithName:buttonFontname size:32.0];
        cell.paidSign.hidden = [self isFreeFontAtIndex:ind];
        
        if (ind == self.currentIndex) {
            cell.layer.borderWidth = 2;
            cell.layer.borderColor = kBorderColorForSelectedItem.CGColor;
        }
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.item == 0) ? CGSizeMake(56, 56) : CGSizeMake(64, 56);
}

@end



