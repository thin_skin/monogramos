//
//  StoreObserver.m
//  Monogramos
//
//  Created by Dmitry Valov on 31.07.16.
//  Copyright 2016 Prolev. All rights reserved.
//

#import "StoreObserver.h"
#import "const.h"
#import "AppManager.h"

@implementation StoreObserver

@synthesize delegate;

#pragma mark SKPaymentTransactionObserver

- (void) failedTransaction: (SKPaymentTransaction *)transaction {
    [delegate transactionDidError:transaction.error];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)recordTransaction:(SKPaymentTransaction *)transaction  {
	
}

- (void)provideContent:(NSString *)identifier {
	
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction {
    
}

- (void) completeTransaction: (SKPaymentTransaction *)transaction {
    [self recordTransaction: transaction];
    [self provideContent: transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    //[NSBundle appStoreReceiptURL];
    [delegate transactionDidFinish:transaction.payment.productIdentifier];
    [[NSNotificationCenter defaultCenter] postNotificationName:kPurchaseCompleteNotification
                                                        object:nil];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    NSArray *ar = queue.transactions;
    for(NSInteger i = 0; i < [ar count]; ++i) {
        SKPaymentTransaction *transaction = (SKPaymentTransaction*)[ar objectAtIndex:i];
        [delegate transactionDidFinish:transaction.payment.productIdentifier];
        [[NSNotificationCenter defaultCenter] postNotificationName:kPurchaseCompleteNotification
                                                            object:nil];
    }
    if([ar count] <= 0) {
        [delegate transactionDidFinish:nil];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    [delegate transactionDidError:error];
}

#pragma mark - SKProductsRequestDelegate
- (void)requestProductData: (NSString *) productID {
    [self requestProductDataForSet:[NSSet setWithObject:productID]];
}

- (void)requestProductDataForSet:(NSSet *)productIdSet {
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdSet];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    [delegate didReceiveResponseWithProducts:response.products];
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdatePriceNotification object:self userInfo:nil];
}

@end

@implementation SKProduct (LocalizedPrice)

- (NSString *)localizedPrice {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:self.priceLocale];
    NSString *formattedString = [numberFormatter stringFromNumber:self.price];
    return formattedString;
}

@end
