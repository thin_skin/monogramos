//
//  NSString+checkTextWidth.h
//  monogramos
//
//  Created by lenka on 3/28/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (checkTextWidth)

+ (CGFloat)getActualFontSizeForLabel:(UILabel *)label;

@end
