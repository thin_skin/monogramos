//
//  UIImage+ReplaceColor.m
//  monogramos
//
//  Created by lenka on 3/2/17.
//  Copyright © 2017 prolev. All rights reserved.
//


#import "UIImage+ReplaceColor.h"
#import "ColorSpaceUtilities.h"

// empirical values
#define kMinimalBrightness 0.18 //0.12
#define kMinimalSaturation 0.04
#define kMinimalTargetBrightness 0.41
#define kMinimalBrightnessForNotWhite 0.56
#define kMinimalBrightnessForDarkGrey 0.36

@implementation UIImage (ReplaceColor)

- (CIFilter*)prepareDataForReplacingColor:(UIColor*)sourceColor withTolerance:(float)tolerance withColor:(UIColor*)replacementColor {
    CGFloat rgbTarget[3], hsvTarget[3];
    [replacementColor getRed:&(rgbTarget[0]) green:&(rgbTarget[1]) blue:&(rgbTarget[2]) alpha:nil];
    [replacementColor getHue:&(hsvTarget[0]) saturation:&(hsvTarget[1]) brightness:&(hsvTarget[2]) alpha:nil]; // rgbToHSV(rgbTarget, hsvTarget);
    
    CGFloat rgbSource[3], hsvSource[3], alpha;
    [sourceColor getRed:&(rgbSource[0]) green:&(rgbSource[1]) blue:&(rgbSource[2]) alpha:nil];
    [sourceColor getHue:&(hsvSource[0]) saturation:&(hsvSource[1]) brightness:&(hsvSource[2]) alpha:&alpha];
//    rgbToHSV(rgbSource, hsvSource);

    float minHueAngle = hsvSource[0] - tolerance, maxHueAngle = hsvSource[0] + tolerance;
    float deltaHue = hsvTarget[0] - hsvSource[0], deltaSat = hsvTarget[1] - hsvSource[1],
    deltaVal = hsvTarget[2] - hsvSource[2];

    const unsigned int size = 64;
    NSUInteger cubeDataSize = size * size * size * sizeof (float) * 4;
    float *cubeData = (float *)malloc (cubeDataSize);
    CGFloat rgb[3], hsv[3];
    float *c = cubeData;
    
    
    // Populate cube with a simple gradient going from 0 to 1
    for (int z = 0; z < size; z++){
        rgb[2] = ((double)z)/(size-1); // Blue value
        for (int y = 0; y < size; y++){
            rgb[1] = ((double)y)/(size-1); // Green value
            for (int x = 0; x < size; x++){
                rgb[0] = ((double)x)/(size-1); // Red value
                // Convert RGB to HSV
                // You can find publicly available rgbToHSV functions on the Internet

                rgbToHSV(rgb, hsv);
                
                // Use the hue value to determine which to make transparent
                // The minimum and maximum hue angle depends on
                // the color you want to remove
//                float alpha = 1;
                CGFloat rgbNew[3];
                rgbNew[0] = rgb[0];
                rgbNew[1] = rgb[1];
                rgbNew[2] = rgb[2];
                
                if (hsv[2] <= kMinimalBrightness) {
                    // for very dark tones - into target too
                    
                    hsv[0] = hsvTarget[0];
                    
                    hsv[1] = hsvTarget[1];
                    
                    if (hsvTarget[2] <= kMinimalTargetBrightness) {
                        hsv[2] = hsvTarget[2];
                    } else {
                        hsv[2] += deltaVal / 5;
                        if (hsv[1] < kMinimalSaturation) // avoid too gray tone for low-saturated recoloring
                            hsv[2] += deltaVal / 2;
                        if (hsv[2] < 0) hsv[2] = 0;
                        if (hsv[2] > 1) hsv[2] = 1;
                    }
                    
                } else if (hsvSource[0] == 0 && hsvSource[1] == 0 && hsv[2] <= kMinimalBrightnessForNotWhite) {
                // for greyish shades - convert all into target

                    hsv[0] = hsvTarget[0];

                    hsv[1] += deltaSat;
                    if (hsv[1] < 0) hsv[1] = 0;
                    if (hsv[1] > 1) hsv[1] = 1;

                    // for light grey shades
//                    if (hsv[2] > kMinimalBrightnessForDarkGrey) {
//                        hsv[1] += hsvTarget[1]; // = hsv[1]  //= MAX(hsv[2], hsvTarget[1]);
//                    } else {
//                        hsv[1] = hsvTarget[1];
//                    }
                    
                    hsv[2] = hsvTarget[2];
                    
                } else {
                    if (hsv[0] > minHueAngle && hsv[0] < maxHueAngle
                        && hsv[1] >= 0.1) {  // to exclude noise from low-saturated pixels

                        hsv[0] += deltaHue;
                        if (hsv[0] < 0) hsv[0] = 0;
                        if (hsv[0] > 1) hsv[0] = 1;

                        hsv[1] += deltaSat;
                        if (hsv[1] < 0) hsv[1] = 0;
                        if (hsv[1] > 1) hsv[1] = 1;
                        
                        hsv[2] += deltaVal;
                        if (hsv[2] < 0) hsv[2] = 0;
                        if (hsv[2] > 1) hsv[2] = 1;

                        if (hsvTarget[1] < kMinimalSaturation) {
                            hsv[1] = hsvTarget[1];
                        }
                    }
                }
                
                hsvToRGB(hsv, rgbNew);
                // Calculate premultiplied alpha values for the cube
                c[0] = rgbNew[0] * alpha;
                c[1] = rgbNew[1] * alpha;
                c[2] = rgbNew[2] * alpha;
                c[3] = alpha;
                c += 4; // advance our pointer into memory for the next color value
            }
        }
    }
    // Create memory with the cube data
    NSData *data = [NSData dataWithBytesNoCopy:cubeData
                                        length:cubeDataSize
                                  freeWhenDone:YES];
    CIFilter *colorCube = [CIFilter filterWithName:@"CIColorCube"
                               withInputParameters:@{
                                                     @"inputCubeDimension": @(size),
                                                     @"inputCubeData": data,
                                                     }];
    return colorCube;
}

- (UIImage *)makeUIImageFromCIImage:(CIImage *)ciImage inContext:(CIContext*)context {
    
    CGImageRef cgImage = [context createCGImage:ciImage fromRect:[ciImage extent]];
    
    UIImage* uiImage = [UIImage imageWithCGImage:cgImage scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(cgImage);
    
    return uiImage;
}


- (UIImage*)imageInConvertedColors:(UIColor*)color intoColor:(UIColor*)replaceColor inContext:(CIContext*)context {
    CIFilter *colorCube = [self prepareDataForReplacingColor:color withTolerance:0.08 withColor:replaceColor];

    [colorCube setValue:[[CIImage alloc] initWithCGImage:self.CGImage] forKey:kCIInputImageKey];
    CIImage *outputImg = colorCube.outputImage;
    return [self makeUIImageFromCIImage:outputImg inContext:context];
}


- (NSMutableDictionary*)mainColoursInImageInDetail:(int)detail {
    
    UIImage *image = self;
    //1. determine detail vars (0==low,1==default,2==high)
    //default detail
    float dimension = 10; //20; //10
    float flexibility = 1; //5; //2
    float range = 60;// 60
    
    //low detail
    if (detail == 0){
        dimension = 10; //4;
        flexibility = 1;//1;
        range = 60;//100;
        
        //high detail (patience!)
    } else if (detail == 2){
        dimension = 25; //100;
        flexibility = 1; //10;
        range = 60; //20;
    }
    
    //2. determine the colours in the image
    NSMutableArray * colours = [NSMutableArray new];
    CGImageRef imageRef = [image CGImage];
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = (unsigned char*) calloc(dimension * dimension * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * dimension;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, dimension, dimension, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, CGRectMake(0, 0, dimension, dimension), imageRef);
    CGContextRelease(context);
    
    float x = 0;
    float y = 0;
    for (int n = 0; n<(dimension*dimension); n++){
        
        int index = (bytesPerRow * y) + x * bytesPerPixel;
        int red   = rawData[index];
        int green = rawData[index + 1];
        int blue  = rawData[index + 2];
        int alpha = rawData[index + 3];
        NSArray * a = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%i",red],[NSString stringWithFormat:@"%i",green],[NSString stringWithFormat:@"%i",blue],[NSString stringWithFormat:@"%i",alpha], nil];
        [colours addObject:a];
        
        y++;
        if (y==dimension){
            y=0;
            x++;
        }
    }
    free(rawData);
    
    //3. add some colour flexibility (adds more colours either side of the colours in the image)
    NSArray * copyColours = [NSArray arrayWithArray:colours];
    NSMutableArray * flexibleColours = [NSMutableArray new];
    
    float flexFactor = flexibility * 2 + 1;
    float factor = flexFactor * flexFactor * 3; //(r,g,b) == *3
    for (int n = 0; n < (dimension * dimension); n++){
        
        NSArray * pixelColours = copyColours[n];
        NSMutableArray * reds = [NSMutableArray new];
        NSMutableArray * greens = [NSMutableArray new];
        NSMutableArray * blues = [NSMutableArray new];
        
        for (int p = 0; p < 3; p++){ // p == 0 stands for r, 1 for g, 2 for b
            
            NSString * rgbStr = pixelColours[p];
            int rgb = [rgbStr intValue];
            
            for (int f = -flexibility; f < flexibility + 1; f++){
                int newRGB = rgb + f;
                if (newRGB < 0){
                    newRGB = 0;
                }
                if (p == 0){
                    [reds addObject:[NSString stringWithFormat:@"%i",newRGB]];
                } else if (p == 1){
                    [greens addObject:[NSString stringWithFormat:@"%i",newRGB]];
                } else if (p == 2){
                    [blues addObject:[NSString stringWithFormat:@"%i",newRGB]];
                }
            }
        }
        
        int r = 0;
        int g = 0;
        int b = 0;
        for (int k = 0; k < factor; k++){
            
            int red = [reds[r] intValue];
            int green = [greens[g] intValue];
            int blue = [blues[b] intValue];
            
            NSString * rgbString = [NSString stringWithFormat:@"%i,%i,%i",red,green,blue];
            [flexibleColours addObject:rgbString];
            
            b++;
            if (b == flexFactor){ b=0; g++; }
            if (g == flexFactor){ g=0; r++; }
        }
    }
    
    //4. distinguish the colours
    //orders the flexible colours by their occurrence
    //then keeps them if they are sufficiently disimilar
    
    NSMutableDictionary * colourCounter = [NSMutableDictionary new];
    
    //count the occurences in the array
    NSCountedSet *countedSet = [[NSCountedSet alloc] initWithArray:flexibleColours];
    for (NSString *item in countedSet) {
        NSUInteger count = [countedSet countForObject:item];
        [colourCounter setValue:[NSNumber numberWithInteger:count] forKey:item];
    }
    
    //sort keys highest occurrence to lowest
    NSArray *orderedKeys = [colourCounter keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2){
        return [obj2 compare:obj1];
    }];
    
    //checks if the colour is similar to another one already included
    NSMutableArray * ranges = [NSMutableArray new];
    for (NSString * key in orderedKeys){
        NSArray * rgb = [key componentsSeparatedByString:@","];
        int r = [rgb[0] intValue];
        int g = [rgb[1] intValue];
        int b = [rgb[2] intValue];
        bool exclude = false;
        for (NSString * ranged_key in ranges){
            NSArray * ranged_rgb = [ranged_key componentsSeparatedByString:@","];
            
            int ranged_r = [ranged_rgb[0] intValue];
            int ranged_g = [ranged_rgb[1] intValue];
            int ranged_b = [ranged_rgb[2] intValue];
            
            if (r>= ranged_r-range && r<= ranged_r+range){
                if (g>= ranged_g-range && g<= ranged_g+range){
                    if (b>= ranged_b-range && b<= ranged_b+range){
                        exclude = true;
                    }
                }
            }
        }
        
        if (!exclude){ [ranges addObject:key]; }
    }
    
    //return ranges array here if you just want the ordered colours high to low
    NSMutableArray * colourArray = [NSMutableArray new];
    for (NSString * key in ranges){
        NSArray * rgb = [key componentsSeparatedByString:@","];
        float r = [rgb[0] floatValue];
        float g = [rgb[1] floatValue];
        float b = [rgb[2] floatValue];
        UIColor * colour = [UIColor colorWithRed:(r/255.0f) green:(g/255.0f) blue:(b/255.0f) alpha:1.0f];
        [colourArray addObject:colour];
    }
    
    //if you just want an array of images of most common to least, return here
    //return [NSDictionary dictionaryWithObject:colourArray forKey:@"colours"];
    
    
    //if you want percentages to colours continue below
    NSMutableDictionary * temp = [NSMutableDictionary new];
    float totalCount = 0.0f;
    for (NSString * rangeKey in ranges){
        NSNumber * count = colourCounter[rangeKey];
        totalCount += [count intValue];
        temp[rangeKey]=count;
    }
    
    //set percentages
    NSMutableDictionary * colourDictionary = [NSMutableDictionary new];
    for (NSString * key in temp){
        float count = [temp[key] floatValue];
        float percentage = count/totalCount;
        NSArray * rgb = [key componentsSeparatedByString:@","];
        float r = [rgb[0] floatValue];
        float g = [rgb[1] floatValue];
        float b = [rgb[2] floatValue];
        UIColor * colour = [UIColor colorWithRed:(r/255.0f) green:(g/255.0f) blue:(b/255.0f) alpha:1.0f];
        colourDictionary[colour]=[NSNumber numberWithFloat:percentage];
    }
    
    return colourDictionary;
    
}

@end
