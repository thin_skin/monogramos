//
//  UIImage+ReplaceColor.h
//  monogramos
//
//  Created by lenka on 3/2/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreImage;

@interface UIImage (ReplaceColor)

- (UIImage*)imageInConvertedColors:(UIColor*)color intoColor:(UIColor*)replaceColor inContext:(CIContext*)context;
- (NSMutableDictionary*)mainColoursInImageInDetail:(int)detail;

@end
