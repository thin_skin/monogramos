//
//  CALayer+GetUIImage.h
//  animationVideo
//
//  Created by lenka on 2/12/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CALayer (GetUIImage)

- (UIImage *)getUIImage;

@end
