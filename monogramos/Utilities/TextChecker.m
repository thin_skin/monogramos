//
//  TextChecker.m
//  monogramos
//
//  Created by lenka on 2/27/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import "TextChecker.h"

@import CoreText;

@implementation TextChecker

+ (BOOL)isCharacter:(unichar)character supportedByFont:(UIFont *)aFont {
    UniChar characters[] = { character };
    CGGlyph glyphs[1] = { };
    CTFontRef ctFont = CTFontCreateWithName((CFStringRef)aFont.fontName, aFont.pointSize, NULL);
    BOOL ret = CTFontGetGlyphsForCharacters(ctFont, characters, glyphs, 1);
    CFRelease(ctFont);
    return ret;
}

@end
