//
//  TextChecker.h
//  monogramos
//
//  Created by lenka on 2/27/17.
//  Copyright © 2017 prolev. All rights reserved.
//

@import UIKit;

@interface TextChecker : NSObject

+ (BOOL)isCharacter:(unichar)character supportedByFont:(UIFont *)aFont;

@end
