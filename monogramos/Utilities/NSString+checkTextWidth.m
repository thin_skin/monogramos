//
//  NSString+checkTextWidth.m
//  monogramos
//
//  Created by lenka on 3/28/17.
//  Copyright © 2017 prolev. All rights reserved.
//

@import UIKit;

#import "NSString+checkTextWidth.h"

@implementation NSString (checkTextWidth)

+ (CGFloat)getActualFontSizeForLabel:(UILabel *)label
{
    CGFloat actualScaleFactor;
    
    CGSize size = [label.text sizeWithAttributes:@{ NSFontAttributeName: label.font }];
    actualScaleFactor = label.bounds.size.width / size.width;
    if (actualScaleFactor > 1) actualScaleFactor = 1;

    CGFloat actualFontSize = label.font.pointSize * actualScaleFactor;
    return actualFontSize;
}

/*
 - (CGFloat)widthWithFont:(UIFont *)font
{
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:self attributes:attributes] size].width;
}

- (CGFloat)heigthWithWidth:(CGFloat)width andFont:(UIFont *)font
{
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:self];
    [attrStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [self length])];
    CGRect rect = [attrStr boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    return rect.size.height;
}
 
 */
@end
