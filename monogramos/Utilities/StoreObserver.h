//
//  StoreObserver.h
//  BubbleHop
//
//  Created by Dmitry Valov on 31.07.16.
//  Copyright 2016 Prolev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@protocol StoreObserverProtocol <NSObject>
-(void)transactionDidFinish:(NSString*)productIdentifier;
-(void)transactionDidError:(NSError*)error;
-(void)didReceiveResponseWithProducts:(NSArray<SKProduct*>*)products;
@end


@interface StoreObserver : NSObject<SKPaymentTransactionObserver, SKProductsRequestDelegate>
{
	id<StoreObserverProtocol> __unsafe_unretained delegate;
    
    SKProduct *product;
    SKProductsRequest *productsRequest;
}

@property (nonatomic, assign) id <StoreObserverProtocol> delegate;

- (void) paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions;
- (void) restoreTransaction: (SKPaymentTransaction *)transaction;
- (void) completeTransaction: (SKPaymentTransaction *)transaction;

- (void) requestProductData: (NSString *) productID;
- (void) requestProductDataForSet:(NSSet *)productIdSet;

@end


@interface SKProduct (LocalizedPrice)

@property (nonatomic, readonly) NSString *localizedPrice;

@end 
