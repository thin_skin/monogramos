//
//  CALayer+GetUIImage.m
//  animationVideo
//
//  Created by lenka on 2/12/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import "CALayer+GetUIImage.h"

@implementation CALayer (GetUIImage)

- (UIImage *)getUIImage {
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        UIGraphicsBeginImageContextWithOptions([self frame].size, NO, [UIScreen mainScreen].scale);
    else
        UIGraphicsBeginImageContext([self frame].size);
    
    [self renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}

@end
