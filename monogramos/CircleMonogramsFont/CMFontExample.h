//
//  CMFontExample.h
//  monogramos
//
//  Created by lenka on 4/4/17.
//  Copyright © 2017 prolev. All rights reserved.
//

- (void)setCircleMonogramText:(NSString*)text withFont:(NSString*)CMFontname {
    NSString *currentLabelFontName = self.vc.monogramLabel.font.fontName;
    if (text.length > [CircleMonogramsFont maxLength] &&
        ![CircleMonogramsFont isOwnFontWithName:currentLabelFontName]) {
        [self.vc showMessage:@"Some text was hidden to match the style of this font." withTitle:nil];
    }
    
    kFrameType frameType = [CircleMonogramsFont frameTypeFromEncodedFontname:CMFontname];
    self.vc.monogramLabel.text = [CircleMonogramsFont prepareString:text withFrame:frameType];
    
    kBackgroundType type = [CircleMonogramsFont typeFromEncodedFontname:CMFontname];
    NSString *actualFontname = [CircleMonogramsFont fontnameForString:text withBackgroundType:type];
    self.vc.monogramLabel.font = [UIFont fontWithName:actualFontname size:self.vc.monogramLabel.font.pointSize];
}



- (void)setFontname:(NSString*)monogramFontname {
    self.currentFontname = monogramFontname;
    NSString *monogramOriginalText = self.vc.monogramTextInput.text;
    
    if ([CircleMonogramsFont isOwnFontWithName:monogramFontname]) {
        [self setCircleMonogramText:monogramOriginalText withFont:monogramFontname];
    } else {
        self.vc.monogramLabel.text = monogramOriginalText;
        self.vc.monogramLabel.font = [UIFont fontWithName:monogramFontname size:self.vc.monogramLabel.font.pointSize];
    }
}

- (void)setTextToMonogram {
    NSString *monogramOriginalText = self.vc.monogramTextInput.text;
    NSString *monogramFontname = self.currentFontname;
    
    if ([CircleMonogramsFont isOwnFontWithName:monogramFontname]) {
        [self setCircleMonogramText:monogramOriginalText withFont:monogramFontname];
    } else {
        self.vc.monogramLabel.text = monogramOriginalText;
    }
}

