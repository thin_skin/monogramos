//
//  CircleMonogramsFont.h
//  monogramos
//
//  Created by lenka on 2/27/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kCMFontWhiteButtonTag   1000
#define kCMFontBlackButtonTag   2000

/*
 Notice: kFrameTypeSquare1 and kFrameTypeSquare3 lookes the same for Circle Monogram ___ White fonts
*/

typedef enum {
    kFrameNone = 0,
    kFrameTypeRound1, // Double Thin Round frame
    kFrameTypeRound2, // Single Thick Round frame
    kFrameTypeRound3, // Single Thin Round frame
    kFrameTypeSquare1, // Single Thick Round frame inside Square
    kFrameTypeSquare2, // Single Thick Round frame inside filled Square
    kFrameTypeSquare3,  // Single Thin Round frame inside Square
    kFrameTypeDotted1,  // Round dotted - thin dots
    kFrameTypeDotted2,  // Round dotted - thick dots
    kFrameTypeCount
} kFrameType;

typedef enum {
    kBackgroundTypeWhite = 0, // colored letters, transparent background
    kBackgroundTypeBlack, // colored background, white letters
    kBackgroundTypeCount
} kBackgroundType;

@interface CircleMonogramsFont : NSObject

+ (BOOL)isOwnFontWithName:(NSString*)fontName;

+ (int)maxLength;

+ (NSString*)encodedFontnameWithType:(kBackgroundType)type andFrame:(kFrameType)frame;
+ (kBackgroundType)typeFromEncodedFontname:(NSString *)fontName;
+ (kFrameType)frameTypeFromEncodedFontname:(NSString*)fontname;

+ (NSString*)fontnameForString:(NSString*)string withBackgroundType:(kBackgroundType)bgtype;
+ (NSString *)prepareString:(NSString *)string withFrame:(kFrameType)frameType;

@end
