//
//  CircleMonogramsFont.m
//  monogramos
//
//  Created by lenka on 2/27/17.
//  Copyright © 2017 prolev. All rights reserved.
//


#import "CircleMonogramsFont.h"

#define kFontID    @"Diagramm"
#define kFrameID   @"Frame"
#define kWhiteTypeID     @"White"
#define kBlackTypeID     @"Black"

#define kCircleFontMaxLength    3


@implementation CircleMonogramsFont

+ (BOOL)isOwnFontWithName:(NSString *)fontName {
    return [fontName containsString:kFontID];
}

+ (int)maxLength {
    return kCircleFontMaxLength;
}

+ (NSString*)encodedFontnameWithType:(kBackgroundType)type andFrame:(kFrameType)frame {
    NSString *typeStr = (type == kBackgroundTypeWhite) ? kWhiteTypeID : kBlackTypeID;
    return [NSString stringWithFormat:@"%@%@%@%d", kFontID, typeStr, kFrameID, frame];
}


+ (kBackgroundType)typeFromEncodedFontname:(NSString *)fontName {
    return [fontName containsString:kWhiteTypeID] ? kBackgroundTypeWhite : kBackgroundTypeBlack;
}

+ (kFrameType)frameTypeFromEncodedFontname:(NSString*)fontname {
    kFrameType frameType = 0;
    NSRange range = [fontname rangeOfString:kFrameID];
    if (range.location != NSNotFound) {
        NSString *frameNum = [fontname substringFromIndex:range.location + range.length];
        if (frameNum.length > 0) frameType = [frameNum intValue];
    }
    return frameType;
}

/*
    Returns corrent font name of given type for original string
    !!! Important: original string. Don't apply to string, returned by method prepareString:withFrame:
*/
+ (NSString*)fontnameForString:(NSString*)string withBackgroundType:(kBackgroundType)bgtype {
    int howManyLetters = string.length > 2 ? 3 : 2;
    NSString *typeStr = (bgtype == kBackgroundTypeWhite) ? kWhiteTypeID : kBlackTypeID;
    return [NSString stringWithFormat:@"%@%d%@", kFontID, howManyLetters, typeStr];
}

/*
    Encoding monogram letters due to manual for Circle Monogram Font:
    first letter should be in lowercase,
    second - in uppercase (and '&' should be replaced by '~' or '`'),
    third (if exists) - replaced by symbol from table.
    Mentioned manual (Circle-Monograms_Read_Me.pdf) included to folder Fonts/Circle Monograms/Circle Monograms 
 
    string - original monogram string, supposed to consist of 2 or 3 symbols, all extra symbols will be trimmed.
    frameType - see the appearance if each type of frame in the same manual.
 
*/

+ (NSString *)prepareString:(NSString *)string withFrame:(kFrameType)frameType {
    if (string.length == 0) return string;
    if (string.length > kCircleFontMaxLength) string = [string substringToIndex:kCircleFontMaxLength];
    
    // first symbol
    NSString *c1 = @"";
    if (string.length > 0) {
        c1 = [[string lowercaseString] substringToIndex:1];
    }
    
    // second symbol
    NSString *c2 = @"";
    if (string.length > 1) {
        NSRange range = [string rangeOfComposedCharacterSequencesForRange:NSMakeRange(1, 1)];
        c2 = [[string uppercaseString] substringWithRange: range];
        if (string.length == 3 && [c2 isEqualToString:@"&"]) c2 = @"~";
    }
    
    // third symbol
    NSString *c3 = @"";
    if (string.length > 2) {
        NSRange range = [string rangeOfComposedCharacterSequencesForRange:NSMakeRange(2, 1)];
        NSString *thirdSymbol = [[string uppercaseString] substringWithRange:range];
        if ([thirdSymbol isEqualToString:@"A"]) {
            c3 = @"1";
        }
        else if ([thirdSymbol isEqualToString:@"B"]) {
            c3 = @"2";
        }
        else if ([thirdSymbol isEqualToString:@"C"]) {
            c3 = @"3";
        }
        else if ([thirdSymbol isEqualToString:@"D"]) {
            c3 = @"4";
        }
        else if ([thirdSymbol isEqualToString:@"E"]) {
            c3 = @"5";
        }
        else if ([thirdSymbol isEqualToString:@"F"]) {
            c3 = @"6";
        }
        else if ([thirdSymbol isEqualToString:@"G"]) {
            c3 = @"7";
        }
        else if ([thirdSymbol isEqualToString:@"H"]) {
            c3 = @"8";
        }
        else if ([thirdSymbol isEqualToString:@"I"]) {
            c3 = @"9";
        }
        else if ([thirdSymbol isEqualToString:@"J"]) {
            c3 = @"0";
        }
        else if ([thirdSymbol isEqualToString:@"K"]) {
            c3 = @"!";
        }
        else if ([thirdSymbol isEqualToString:@"L"]) {
            c3 = @"@";
        }
        else if ([thirdSymbol isEqualToString:@"M"]) {
            c3 = @"#";
        }
        else if ([thirdSymbol isEqualToString:@"N"]) {
            c3 = @"$";
        }
        else if ([thirdSymbol isEqualToString:@"O"]) {
            c3 = @"%";
        }
        else if ([thirdSymbol isEqualToString:@"P"]) {
            c3 = @"^";
        }
        else if ([thirdSymbol isEqualToString:@"Q"]) {
            c3 = @"&";
        }
        else if ([thirdSymbol isEqualToString:@"R"]) {
            c3 = @"*";
        }
        else if ([thirdSymbol isEqualToString:@"S"]) {
            c3 = @"(";
        }
        else if ([thirdSymbol isEqualToString:@"T"]) {
            c3 = @")";
        }
        else if ([thirdSymbol isEqualToString:@"U"]) {
            c3 = @"-";
        }
        else if ([thirdSymbol isEqualToString:@"V"]) {
            c3 = @"=";
        }
        else if ([thirdSymbol isEqualToString:@"W"]) {
            c3 = @"[";
        }
        else if ([thirdSymbol isEqualToString:@"X"]) {
            c3 = @"]";
        }
        else if ([thirdSymbol isEqualToString:@"Y"]) {
            c3 = @"\\";
        }
        else if ([thirdSymbol isEqualToString:@"Z"]) {
            c3 = @";";
        }
    }
    
    // frame if needed
    NSString *frameSign = @"";
    switch (frameType) {
        case kFrameTypeRound1:
            frameSign = @"<";
            break;
            
        case kFrameTypeRound2:
            frameSign = @"?";
            break;
            
        case kFrameTypeRound3:
            frameSign = @"/";
            break;
            
        case kFrameTypeSquare1:
            frameSign = @">";
            break;
            
        case kFrameTypeSquare2:
            frameSign = @".";
            break;
            
        case kFrameTypeSquare3:
            frameSign = @",";
            break;
         
        case kFrameTypeDotted1:
            frameSign = @"ù"; //@"\u00F9";
            break;
            
        case kFrameTypeDotted2:
            frameSign = @"ü"; //@"\u0250t";
            break;
            
        default:
            break;
    }
    
    NSString *encodedString = [NSString stringWithFormat:@"%@%@%@%@", frameSign, c1, c2, c3];
    return encodedString;
}


@end
