//
//  EditorViewDataSource.h
//  monogramos
//
//  Created by lenka on 3/13/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

#pragma mark - data source classes

@interface ColorsCollectionDataSource : NSObject <UICollectionViewDataSource>
@property NSMutableArray *colors;
@end

@interface BackgroundColorsCollectionDataSource : ColorsCollectionDataSource
@end

@interface ShapeColorsCollectionDataSource : BackgroundColorsCollectionDataSource
@end

@interface BackgroundCategoriesCollectionDataSource : NSObject <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property NSArray *categories;
@property BOOL isUnlocked;
@property NSUInteger currentIndex;
- (instancetype)initWithCategories:(NSArray*)categoriesArray;
- (BOOL)isFreeCategoryAtNumber:(NSUInteger)ind;
@end

@interface BackgroundsCollectionDataSource : NSObject <UICollectionViewDataSource>
@property NSMutableArray *patterns;
@property NSUInteger freeCount;
@property BOOL isUnlocked;
@property BOOL isForSelectedCategory;
@property NSUInteger currentIndex;
- (void)setCategoryName:(NSString*)categoryName withMaxIndex:(NSNumber*)maxIndex withFreeCount:(NSNumber*)freeCount;
- (NSInteger)realPatternIndexForIndex:(NSUInteger)index;
- (BOOL)isFreePatternAtIndex:(NSUInteger)index;
@end

@interface ShapesCollectionDataSource : NSObject <UICollectionViewDataSource>
@property NSMutableArray *shapes;
@property NSUInteger freeCount;
@property BOOL isUnlocked;
@property NSUInteger currentIndex;
- (BOOL)isFreeShapeAtIndex:(NSUInteger)index;
@end

@interface FontsCollectionDataSource : NSObject <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property NSMutableArray *fonts;
@property NSUInteger freeCount;
@property BOOL isUnlocked;
@property NSUInteger currentIndex;
- (BOOL)isFreeFontAtIndex:(NSUInteger)index;
@end

#pragma mark - custom cell classes
@interface FontsCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *modelLabel;
@property (weak, nonatomic) IBOutlet UIImageView *paidSign;
@end

@interface BackgroundCategoriesCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *catImage;
@property (weak, nonatomic) IBOutlet UILabel *catName;
@property (weak, nonatomic) IBOutlet UIImageView *paidSign;
@end

@interface BackgroundsCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *backgroundDesign;
@property (weak, nonatomic) IBOutlet UIImageView *paidSign;
@end

@interface ShapesCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *paidSign;
@property (weak, nonatomic) IBOutlet UIImageView *shapeImage;
@end

@interface BgColorCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *whiteCorner;
@end

//@interface ShapeColorCollectionCell : UICollectionViewCell
//@end
//
