//
//  AppDelegate.m
//  monogramos
//
//  Created by lenka on 2/25/17.
//  Copyright © 2017 prolev. All rights reserved.
//

#import "AppDelegate.h"
#import "AppManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
//    [self printOutFontsNames];
    [AppManager sharedInstance];
    
    //Store Observer
    observer = [[StoreObserver alloc] init];
    observer.delegate = self;
    [[SKPaymentQueue defaultQueue] addTransactionObserver:observer];
    [self requestPrices];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - InAppPurchases
- (void) requestPrices {
    NSSet *productIdSet = [NSSet setWithObjects:
                           kPurchaseID_Wallpapers,
                           kPurchaseID_Badges,
                           kPurchaseID_Fonts,
                           kPurchaseID_AdsFree, nil];
    
    [observer requestProductDataForSet:productIdSet];
}

- (void) transactionDidError:(NSError*)error {
    //[self cancelLoadingAlert];
    
    if(error != nil) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kErrorNotification
                                                            object:nil userInfo:@{@"message" : [error localizedDescription]}];
    }
}

- (void) transactionDidFinish:(NSString*)transactionIdentifier {
    //[self cancelLoadingAlert];
}

- (void) purchase:(NSString *) purchase_id {
    if (![SKPaymentQueue canMakePayments]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kErrorNotification
                                                            object:nil userInfo:@{@"messsage" : @"in-App purchases are disabled"}];
        return;
    }
    
    SKProduct *proUpgradeProduct = [[AppManager sharedInstance].products objectForKey:purchase_id];
    if(proUpgradeProduct != nil)
    {
        SKPayment *payment = [SKPayment paymentWithProduct:proUpgradeProduct];
        
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        [self showWaitingAlert];
    }
}

- (void) restorePurchases {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    [self showWaitingAlert];
}

- (void) showWaitingAlert {
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (sysVer < 7) {
        loadingView = [[UIAlertView alloc] initWithTitle:@"" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
        
        UIActivityIndicatorView *actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        actInd.frame = CGRectMake(128.0f, 45.0f, 25.0f, 25.0f);
        [loadingView addSubview:actInd];
        [actInd startAnimating];
        [actInd release];
        
        UILabel *l = [[UILabel alloc]init];
        l.frame = CGRectMake(100, -25, 210, 100);
        l.text = @"Please wait...";
        l.font = [UIFont fontWithName:@"Helvetica" size:16];
        l.textColor = [UIColor whiteColor];
        l.shadowColor = [UIColor blackColor];
        l.shadowOffset = CGSizeMake(1.0, 1.0);
        l.backgroundColor = [UIColor clearColor];
        [loadingView addSubview:l];
        [l release];
        
        [loadingView show];
        [loadingView release];
    }
    else {
        [MBProgressHUD showHUDAddedTo:navController_.view animated:YES];
    }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    CCLOG(@"[ IN APP PURCHASE] request error: %@", [error localizedDescription]);
}

- (void) transactionDidError {
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (sysVer < 7) {
        [loadingView dismissWithClickedButtonIndex:0 animated:NO];
    }
    else {
        [MBProgressHUD hideHUDForView:navController_.view animated:YES];
    }
}

- (void) cancelLoadingAlert {
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (sysVer < 7) {
        [loadingView dismissWithClickedButtonIndex:0 animated:NO];
    }
    else {
        [MBProgressHUD hideHUDForView:navController_.view animated:YES];
    }
}

@end
